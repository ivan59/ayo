<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\middlewares\CBAuthAPI;
use crocodicstudio\crudbooster\middlewares\CBBackend;

Route::middleware(CBBackend::class)->group(function () {
	CB::routeController(config("crudbooster.ADMIN_PATH").'/ajax',"AdminAjaxController");
	Route::get(config("crudbooster.ADMIN_PATH").'/stats_register',"DashboardController@getStatsRegister");
});
CRUDBooster::routeController('/','FrontController');
try {
	CB::routeController("download","DownloadController");
	CB::routeController(config("crudbooster.ADMIN_PATH")."/dashboard","AdminDashboardController");
    //CB::routeController('/', 'FrontController');
	Route::get('/', function () {
		return redirect('admin');
	});
} catch (Exception $e) {
}
