<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command("clear_transaction",function() {
   $commands = "api_logs;
cash_transfer_histories;
cms_logs;
competitor_product_reports;
courier_tasks;
courier_task_orders;
fcm_logs;
notifications;
orders;
order_details;
order_history_payments;
product_promotions;
product_returns;
sales_plan_reals;
visit_plan_billings;";
    foreach(explode(";", $commands) as $table) {
        $table = trim($table);
        if($table) {
            if(Schema::hasTable($table)) {
                $this->info($table);
                DB::table($table)->delete();
            }
        }
    }

    //clear visit outplan incident
    DB::table("visit_plan_outlets")->where("is_incident",1)->delete();
    DB::table("visit_plan_outlet_dates")->where("is_incident",1)->delete();
    DB::table("visit_plan_outlet_dates")
        ->where("is_visited",1)
        ->update([
           'is_visited'=>null,
           'visited_at'=>null,
           'visited_signature'=>null,
            'return_deduct_type'=>null,
            'visited_latitude'=>null,
            'visited_longitude'=>null
        ]);
    DB::table("visit_plan_outlets")->update([
       "is_new"=>null,
       "is_selling"=>null
    ]);

    $this->info("Cleared");
});

Artisan::command("clear", function () {
   $commands = "api_logs;
cash_transfer_histories;
claims;
cms_logs;
messages;
orders;
order_details;
order_history_payments;
sales_plans;
sales_plan_categories;
sales_plan_cat_products;
sales_plan_reals;
courier_tasks;
courier_task_orders;
competitor_product_reports;
discounts;
discount_outlets;
discount_products;
fcm_logs;
notifications;
outlet_product_discs;
product_promotions;
product_returns;
visit_plans;
visit_plan_billings;
visit_plan_outlets;
visit_plan_outlet_dates;";
   foreach(explode(";", $commands) as $table) {
       $table = trim($table);
       if($table) {
           if(Schema::hasTable($table)) {
               $this->info($table);
               DB::table($table)->delete();
           }
       }
   }
   $this->info("Cleared");
});

Artisan::command("order_check_expired",function () {
    $this->info("Order Check Expired");

    $data = \App\Repos\Orders::findAllExpired(3);

    if($data) {
        $regids = [];
        foreach($data as $row) {
            $order = new \App\Repos\Orders($row);
            $regids[] = $order->getOutlets()->getCmsUsers()->getRegid();
        }
        $regids = array_filter($regids);
        if(count($regids)>0) {
            $fcm = \crocodicstudio\crudbooster\helpers\CB::sendFCM($regids,[
                'title'=>'Anda memiliki order yang belum terbayar',
                'content'=>'Klik disini untuk melihat lebih lengkap',
                'action'=>'list_order'
            ]);
            \Log::debug($fcm);
        }
        $this->info("Regids: " . implode(", ",$regids));
    }
});

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');
