<?php

namespace crocodicstudio\crudbooster\middlewares;

use App\Http\Controllers\Apis\SessionID;
use App\Http\Controllers\CBHook;
use Closure;
use CRUDBooster;
use Illuminate\Http\Response;

class CBAuthAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        CBHook::beforeAuthAPIMiddleware($request);
        
        CRUDBooster::authAPI();

        $start = microtime(true);

        /**
         * @var Response $response
         */
        $response = $next($request);

        $end = microtime(true);

        CBHook::afterAuthAPIMiddleware($request, $response, $end-$start);

        return $response;
    }
}
