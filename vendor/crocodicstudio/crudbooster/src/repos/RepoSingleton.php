<?php namespace crocodicstudio\crudbooster\repos;

class RepoSingleton
{

    private $data;

    public function setRepo($repoClassName, $repoMethodName, $repoId, $data)
    {
        
        $this->data[$repoClassName][$repoMethodName][$repoId] = $data;
    }

    public function getRepo($repoClassName, $repoMethodName, $repoId) {

        return @$this->data[$repoClassName][$repoMethodName][$repoId];
    }
}