<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ ($page_title)?Session::get('appname').': '.strip_tags($page_title):"Admin Area" }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name='generator' content='CRUDBooster 5.4.6'/>
    <meta name='robots' content='noindex,nofollow'/>
    <link rel="shortcut icon"
    href="{{ CRUDBooster::getSetting('favicon')?asset(CRUDBooster::getSetting('favicon')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("vendor/crudbooster/assets/adminlte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="{{asset("vendor/crudbooster/assets/adminlte/font-awesome/css")}}/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="{{asset("vendor/crudbooster/ionic/css/ionicons.min.css")}}" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="{{ asset("vendor/crudbooster/assets/adminlte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("vendor/crudbooster/assets/adminlte/dist/css/skins/_all-skins.min.css")}}" rel="stylesheet" type="text/css"/>
    @yield('custom-head')
    <!-- support rtl-->
    @if (in_array(App::getLocale(), ['ar', 'fa']))
    <link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css">
    <link href="{{ asset("vendor/crudbooster/assets/rtl.css")}}" rel="stylesheet" type="text/css"/>
    @endif

    <link rel='stylesheet' href='{{asset("vendor/crudbooster/assets/css/main.css").'?r='.time()}}'/>

    <!-- load css -->
    <style type="text/css">
        @if($style_css)
        {!! $style_css !!}
        @endif
    </style>
    @if($load_css)
    @foreach($load_css as $css)
    <link href="{{$css}}" rel="stylesheet" type="text/css"/>
    @endforeach
    @endif

    <style type="text/css">
        .datepicker {
            border-radius: 0px !important;
        }
        .dropdown-menu-action {
            left: -130%;
        }

        .btn-group-action .btn-action {
            cursor: default
        }

        #box-header-module {
            box-shadow: 10px 10px 10px #dddddd;
        }

        .sub-module-tab li{
            background: #F9F9F9;
            cursor: pointer;
        }

        .sub-module-tab li.active{
            background: #ffffff;
            box-shadow: 0px -5px 10px #cccccc
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            border: none;
        }

        .nav-tabs > li > a {
            border: none;
        }

        .breadcrumb {
            margin: 0 0 0 0;
            padding: 0 0 0 0;
        }

        .form-group > label:first-child {
            display: block
        }

        .table-responsive table thead tr th {
            white-space: nowrap;
        }
        .table-responsive table tbody tr td {
            white-space: nowrap;
        }
        .form-horizontal .control-label {
            text-align: left;
            font-weight: normal;
        }
        #parent-form-area .form-group {
            border-bottom: 1px solid #fafafa;
        }

        .label {
            padding: 3px 20px 3px 20px;
            /* display: block; */
            font-weight: normal;
            font-size: 13px;
        }

        .table-nowrap {
            overflow: auto;
        }

        .table-nowrap table thead tr th {
            white-space: nowrap;
        }
        .table-nowrap table tbody tr td {
            white-space: nowrap;
        }
        .datatables-head-center thead tr th {
            text-align: center;
            vertical-align: middle;
        }
        .form-group {
            border-bottom: 1px solid #eeeeee;
            padding-bottom: 10px;
        }
        .form-group:last-child {
            border-bottom: 0px;
        }

        table.table-bordered{
            border:1px solid #AAAAAA;
            margin-top:20px;
        }
        table.table-bordered > thead > tr > th{
            border:1px solid #AAAAAA;
        }
        table.table-bordered > tbody > tr > td{
            border:1px solid #AAAAAA;
        }
        table.table-bordered > foot > tr > th {
            border:1px solid #AAAAAA;
        }
        .datatables-simple thead tr th {
            border: 1px solid #AAAAAA !important;
        }
        .datatables-simple tbody tr td {
            border: 1px solid #AAAAAA;
        }

        .alert-info {
            background-color: #00c0ef21 !important;
            border-color: #00acd63d !important;
            color: #00acd6 !important;
        }

    </style>

    @stack('head')
</head>

@if(Request::segment(2)=='day_off_setting' OR Request::segment(2)=='report_time_sheet' OR Request::segment(2)=='report_time_charge' OR Request::segment(2)=='clients')
<body class="@php echo (Session::get('theme_color'))?:'skin-blue'; echo ' '; echo config('crudbooster.ADMIN_LAYOUT'); @endphp sidebar-collapse">
    @elseif(Request::segment(2)=='pembayaran_hutang' && Request::segment(3)!='')
    <body class="@php echo (Session::get('theme_color'))?:'skin-blue'; echo ' '; echo config('crudbooster.ADMIN_LAYOUT'); @endphp sidebar-collapse">
        @else
        <body class="@php echo (Session::get('theme_color'))?:'skin-blue'; echo ' '; echo config('crudbooster.ADMIN_LAYOUT'); @endphp {{($sidebar_mode)?:''}}">
            @endif

            <div id='app' class="wrapper">

                <!-- Header -->
                @include('crudbooster::header')

                <!-- Sidebar -->
                @include('crudbooster::sidebar')

                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">

                    <section class="content-header">
                        <?php
                        $module = CRUDBooster::getCurrentModule();
                        ?>
                        @if($module)
                        <h1>
                            <i class='{{$module->icon}}'></i> {{($page_title)?:$module->name}} &nbsp;&nbsp;

                            <!--START BUTTON -->

                            @if(CRUDBooster::getCurrentMethod() == 'getIndex')
                            @if($button_show)
                            <a href="{{ CRUDBooster::mainpath().'?'.http_build_query(Request::all()) }}" id='btn_show_data' class="btn btn-sm btn-primary"
                            title="{{trans('crudbooster.action_show_data')}}">
                            <i class="fa fa-table"></i> {{trans('crudbooster.action_show_data')}}
                        </a>
                        @endif

                        @if($button_add && CRUDBooster::isCreate())
                        <a href="{{ CRUDBooster::mainpath('add').'?return_url='.urlencode(Request::fullUrl()).'&parent_id='.g('parent_id').'&parent_field='.$parent_field }}"
                        id='btn_add_new_data' class="btn btn-sm btn-success" title="{{trans('crudbooster.action_add_data')}}">
                        <i class="fa fa-plus-circle"></i> {{trans('crudbooster.action_add_data')}}
                    </a>
                    @endif
                    @if(Request::segment(2) == 'notification')
                    <a href="{{ action('AdminNotificationController@getAddData') }}" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i>&nbsp; Add Data</a>
                    @endif
                    @endif


                    @if($button_export && CRUDBooster::getCurrentMethod() == 'getIndex')
                    <a href="javascript:void(0)" id='btn_export_data' data-url-parameter='{{$build_query}}' title='Export Data'
                    class="btn btn-sm btn-primary btn-export-data">
                    <i class="fa fa-upload"></i> {{trans("crudbooster.button_export")}}
                </a>
                @endif

                @if($button_import && CRUDBooster::getCurrentMethod() == 'getIndex')
                <a href="{{ CRUDBooster::mainpath('import-data') }}" id='btn_import_data' data-url-parameter='{{$build_query}}' title='Import Data'
                class="btn btn-sm btn-primary btn-import-data">
                <i class="fa fa-download"></i> {{trans("crudbooster.button_import")}}
            </a>
            @endif

            <!--ADD ACTIon-->
            @if(isset($index_button) && count($index_button))

            @foreach($index_button as $ib)
            <a href='{{$ib["url"]}}' id='{{str_slug($ib["label"])}}' class='btn {{($ib['color'])?'btn-'.$ib['color']:'btn-primary'}} btn-sm'
            @if($ib['onClick']) onClick='return {{$ib["onClick"]}}' @endif
            @if($ib['onMouseOver']) onMouseOver='return {{$ib["onMouseOver"]}}' @endif
            @if($ib['onMouseOut']) onMouseOut='return {{$ib["onMouseOut"]}}' @endif
            @if($ib['onKeyDown']) onKeyDown='return {{$ib["onKeyDown"]}}' @endif
            @if($ib['onLoad']) onLoad='return {{$ib["onLoad"]}}' @endif
            >
            <i class='{{$ib["icon"]}}'></i> {{$ib["label"]}}
        </a>
        @endforeach
        @endif

        <!-- employee active -->
        @if(Request::segment(2)=='employees' && Request::segment(3)=='')
            <a data-toggle="modal" data-target="#exportPDF" class="btn btn-danger btn-sm"><i class="fa fa-download"></i>&nbsp; Export PDF</a>
                <div class="modal fade in" tabindex="-1" role="dialog" id="exportPDF">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                                <h4 class="modal-title"><i class="fa fa-download"></i> &nbsp; Export Data PDF</h4>
                            </div>
                            <form action="{{ action('AdminEmployeesController@postExportPdf') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label style="font-size: 14px;">File Name</label>
                                        <input type="text" name="filename" value="Employees - {{ date('d M Y_H:i:s') }}" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label style="font-size: 14px;">Limit Data</label>
                                        <input type="number" name="ld" class="form-control" value="50" required="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            <a data-toggle="modal" data-target="#exportExcel" class="btn btn-info btn-sm"><i class="fa fa-download"></i>&nbsp; Export Excel</a>
                <div class="modal fade in" tabindex="-1" role="dialog" id="exportExcel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                                <h4 class="modal-title"><i class="fa fa-download"></i> &nbsp; Export Data Excel</h4>
                            </div>
                            <form action="{{ action('AdminEmployeesController@postExportExcel') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label style="font-size: 14px;">File Name</label>
                                        <input type="text" name="filename" value="Employees - {{ date('d M Y_H:i:s') }}" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label style="font-size: 14px;">Limit Data</label>
                                        <input type="number" name="ld" class="form-control" value="50" required="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        @endif

        <!-- filter status module report leave -->
        @if(Request::segment(2)=='leaves' && Request::segment(3)=='')
            <a data-toggle="modal" data-target="#filter_leave" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i>&nbsp; Filter Status</a>
                <div class="modal fade in" tabindex="-1" role="dialog" id="filter_leave">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                                <h4 class="modal-title"><i class="fa fa-filter"></i> &nbsp; Filter leaves by status</h4>
                            </div>
                            <div class="modal-body">
                                <center>
                                    <a href="{{url('admin/leaves')}}" class="btn btn-md btn-primary">All</a>
                                    <a href="{{url('admin/leaves?status=2')}}" class="btn btn-md btn-warning">Waiting</a>
                                    <a href="{{url('admin/leaves?status=1')}}" class="btn btn-md btn-success">Approved</a>
                                    <a href="{{url('admin/leaves?status=0')}}" class="btn btn-md btn-danger">Rejected</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
        @endif

        <!-- filter status module report overtime -->
        @if(Request::segment(2)=='overtime' && Request::segment(3)=='')
            <a data-toggle="modal" data-target="#filter_leave" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i>&nbsp; Filter Status</a>
                <div class="modal fade in" tabindex="-1" role="dialog" id="filter_leave">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                                <h4 class="modal-title"><i class="fa fa-download"></i> &nbsp; Filter by status</h4>
                            </div>
                            <div class="modal-body">
                                <center>
                                    <a href="{{url('admin/overtime')}}" class="btn btn-md btn-primary">All</a>
                                    <a href="{{url('admin/overtime?status=2')}}" class="btn btn-md btn-warning">Waiting</a>
                                    <a href="{{url('admin/overtime?status=1')}}" class="btn btn-md btn-success">Approved</a>
                                    <a href="{{url('admin/overtime?status=0')}}" class="btn btn-md btn-danger">Rejected</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
        @endif

        <!-- employee resign -->
        @if(Request::segment(2)=='resign_employee' && Request::segment(3)=='')
            <a data-toggle="modal" data-target="#exportPDF" class="btn btn-danger btn-sm"><i class="fa fa-download"></i>&nbsp; Export PDF</a>
                <div class="modal fade in" tabindex="-1" role="dialog" id="exportPDF">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                                <h4 class="modal-title"><i class="fa fa-download"></i> &nbsp; Export Data PDF</h4>
                            </div>
                            <form action="{{ action('AdminResignEmployeeController@postExportPdf') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label style="font-size: 14px;">File Name</label>
                                        <input type="text" name="filename" value="Employees - {{ date('d M Y_H:i:s') }}" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label style="font-size: 14px;">Limit Data</label>
                                        <input type="number" name="ld" class="form-control" value="50" required="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            <a data-toggle="modal" data-target="#exportExcel" class="btn btn-info btn-sm"><i class="fa fa-download"></i>&nbsp; Export Excel</a>
                <div class="modal fade in" tabindex="-1" role="dialog" id="exportExcel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal"><span aria-hidden="true">x</span></button>
                                <h4 class="modal-title"><i class="fa fa-download"></i> &nbsp; Export Data Excel</h4>
                            </div>
                            <form action="{{ action('AdminResignEmployeeController@postExportExcel') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label style="font-size: 14px;">File Name</label>
                                        <input type="text" name="filename" value="Employees - {{ date('d M Y_H:i:s') }}" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label style="font-size: 14px;">Limit Data</label>
                                        <input type="number" name="ld" class="form-control" value="50" required="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        @endif

        <!-- END BUTTON -->
    </h1>

    <ol class="breadcrumb">
        <li><a href="{{CRUDBooster::adminPath()}}"><i class="fa fa-dashboard"></i> {{ trans('crudbooster.home') }}</a></li>
        <li class="active">{{$module->name}}</li>
    </ol>
    @else
    <h1>{{ isset($page_title)?$page_title:Session::get('appname')}}
        @if(!$page_title)<small>Information</small>@endif
    </h1>
    @endif
</section>


<!-- Main content -->
<section id='content_section' class="content">

    @if(@$alerts)
    @foreach(@$alerts as $alert)
    <div class='callout callout-{{$alert["type"]}}'>
        {!! $alert['message'] !!}
    </div>
    @endforeach
    @endif


    @if (Session::get('message')!='')
    <div class='alert alert-{{ Session::get("message_type") }}'>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-info"></i> {{ trans("crudbooster.alert_".Session::get("message_type")) }}</h4>
        {!!Session::get('message')!!}
    </div>
    @endif