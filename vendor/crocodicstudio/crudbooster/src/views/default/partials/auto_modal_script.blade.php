@if(isset($forms) && count($forms)>0 && ((count($forms)<6 && $auto_modal_form) || $modal_form===true))
    <?php
    $textareaTest = false;
    foreach($forms as $form) {
        if($form['type']=='textarea' || $form['type'] == 'wysiwyg') {
            $textareaTest = true;
            break;
        }
    }
    ?>
    <div class="modal" id="modal-form">
        <div class="modal-dialog {{ $textareaTest?'modal-lg':'' }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form</h4>
                </div>
                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {!! csrf_field() !!}
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script>
        $(function () {
            $('#btn_add_new_data').click(function (e){
                e.preventDefault();
                $('#modal-form .modal-body').html("<h3 align='center'><i class='fa fa-spin fa-spinner'></i> Please wait loading form...</h3>");
                $('#modal-form').modal('show');
                $.get($(this).attr('href'),function (response) {
                    let url = "{{ CB::mainpath('add-save') }}";
                    let form = $(response).find('#parent-form-area').html();
                    $('#modal-form form').attr('action',url);
                    $('#modal-form .modal-title').text('Add Data');
                    $('#modal-form .modal-body').html(form);
                    // $('#modal-form .modal-body .form-group .col-sm-9').removeClass('col-sm-9').addClass('col-sm-10');
                })
            })

            $('#table_dashboard .btn-edit').click(function (e){
                e.preventDefault();
                $('#modal-form .modal-body').html("<h3 align='center'><i class='fa fa-spin fa-spinner'></i> Please wait loading form...</h3>");
                $('#modal-form').modal('show');
                let id = $(this).data('id');
                $.get($(this).attr('href'),function (response) {
                    let url = "{{ CB::mainpath('edit-save') }}/"+id;
                    let form = $(response).find('#parent-form-area').html();
                    $('#modal-form form').attr('action',url);
                    $('#modal-form .modal-title').text('Edit Data');
                    $('#modal-form .modal-body').html(form);
                    // $('#modal-form .modal-body .form-group .col-sm-9').removeClass('col-sm-9').addClass('col-sm-10');
                })
            })
        })
    </script>
@endif