<style type="text/css">
    .bootstrap-timepicker .dropdown-menu {
        left: 185px !important;
        box-shadow: 0px 0px 20px #aaaaaa;
    }
</style>
<script>
    $(function () {
        if($(".timepicker").length > 0) {
            $(".timepicker").timepicker({
                showInputs: true,
                showSeconds: true,
                showMeridian:false
            });
        }
    })
</script>