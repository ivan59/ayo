<script type="text/javascript">
    $(function () {
        $('#{{$parent}}, input:radio[name={{$parent}}]').change(function () {
            var $current = $("#{{$form['name']}}");
            var parent_id = $(this).val();
            var fk_name = "{{$parent}}";
            var fk_value = $(this).val();
            var datatable = "{{$form['datatable']}}".split(',');
                    @if(!empty($add_field))
            var add_field = ($("#{{$add_field}}").val()) ? $("#{{$add_field}}").val() : "";
                    @endif
            var datatableWhere = "{{$form['datatable_where']}}";
            @if(!empty($add_field))
            if (datatableWhere) {
                if (add_field) {
                    datatableWhere = datatableWhere + " and {{$add_field}} = " + add_field;
                }
            } else {
                if (add_field) {
                    datatableWhere = "{{$add_field}} = " + add_field;
                }
            }
                    @endif
            var table = datatable[0].trim('');
            var label = datatable[1].trim('');
            var value = "{{$value}}";

            if (fk_value != '') {
                $current.html("<option value=''>{{trans('crudbooster.text_loading')}} {{$form['label']}}");
                $.get("{{CRUDBooster::mainpath('data-table')}}?table=" + table + "&label=" + label + "&fk_name=" + fk_name + "&fk_value=" + fk_value + "&datatable_where=" + encodeURI(datatableWhere), function (response) {
                    if (response) {
                        $current.html("<option value=''>{{$default}}");
                        $.each(response, function (i, obj) {
                            var selected = (value && value == obj.select_value) ? "selected" : "";
                            $("<option " + selected + " value='" + obj.select_value + "'>" + obj.select_label + "</option>").appendTo("#{{$form['name']}}");
                        })
                        $current.trigger('change');
                    }
                });
            } else {
                $current.html("<option value=''>{{$default}}");
            }
        })

        $('#{{$parent}}').trigger('change');
        $("input[name='{{$parent}}']:checked").trigger("change");
        $("#{{$form['name']}}").trigger('change');
    })
</script>