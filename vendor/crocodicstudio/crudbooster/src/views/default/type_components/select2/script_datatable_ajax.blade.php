<script>
    $(function () {
        $('#{{$name}}').select2({
            placeholder: {
                id: '-1',
                text: '{{trans('crudbooster.text_prefix_option')}} {{$form['label']}}'
            },
            allowClear: true,
            ajax: {
                url: '{!! $url !!}',
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                        format: "{{$format}}",
                        table1: "{{$table1}}",
                        column1: "{{$column1}}",
                        table2: "{{$table2}}",
                        column2: "{{$column2}}",
                        table3: "{{$table3}}",
                        column3: "{{$column3}}",
                        where: "{!! addslashes($where) !!}"
                    }
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data.items
                    };
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            @if($value)
            initSelection: function (element, callback) {
                var id = $(element).val() ? $(element).val() : "{{$value}}";
                if (id !== '') {
                    $.ajax('{{$url}}', {
                        data: {
                            id: id,
                            format: "{{$format}}",
                            table1: "{{$table1}}",
                            column1: "{{$column1}}",
                            table2: "{{$table2}}",
                            column2: "{{$column2}}",
                            table3: "{{$table3}}",
                            column3: "{{$column3}}"
                        },
                        dataType: "json"
                    }).done(function (data) {
                        callback(data.items[0]);
                        $('#<?php echo $name?>').html("<option value='" + data.items[0].id + "' selected >" + data.items[0].text + "</option>");
                    });
                }
            }

            @endif
        });

    })
</script>