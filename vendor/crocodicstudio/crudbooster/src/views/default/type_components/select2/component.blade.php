@if($form['datatable'])

    @if($form['relationship_table'])
        @if(request()->ajax())
            <script type="text/javascript">
                $(function () {
                    $('#{{$name}}').select2();
                })
            </script>
        @else
            @push('bottom')
                <script type="text/javascript">
                    $(function () {
                        $('#{{$name}}').select2();
                    })
                </script>
            @endpush
        @endif
    @else
        @if($form['datatable_ajax'] == true)

            <?php
            $datatable = @$form['datatable'];
            $where = @$form['datatable_where'];
            $format = @$form['datatable_format'];

            $raw = explode(',', $datatable);
            $url = CRUDBooster::mainpath("find-data");

            $table1 = $raw[0];
            $column1 = $raw[1];

            @$table2 = $raw[2];
            @$column2 = $raw[3];

            @$table3 = $raw[4];
            @$column3 = $raw[5];
            ?>

            @if(request()->ajax())
                @include('crudbooster::default.type_components.select2.script_datatable_ajax')
            @else
                @push('bottom')
                    @include('crudbooster::default.type_components.select2.script_datatable_ajax')
                @endpush
            @endif

        @else
            @if(request()->ajax())
                <script type="text/javascript">
                    $(function () {
                        $('#{{$name}}').select2();
                    })
                </script>
            @else
                @push('bottom')
                    <script type="text/javascript">
                        $(function () {
                            $('#{{$name}}').select2();
                        })
                    </script>
                @endpush
            @endif
        @endif
    @endif
@else

    @if(request()->ajax())
        <script type="text/javascript">
            $(function () {
                $('#{{$name}}').select2();
            })
        </script>
    @else
        @push('bottom')
            <script type="text/javascript">
                $(function () {
                    $('#{{$name}}').select2();
                })
            </script>
        @endpush
    @endif

@endif

<div class='form-group {{$header_group_class}} {{ ($errors->first($name))?"has-error":"" }}' id='form-group-{{$name}}' style="{{@$form['style']}}">
    <label class='control-label col-sm-{{$form_label_width }}'>{{$form['label']}}
        @if($required)
            <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
        @endif
    </label>
    <div class="{{$col_width?:'col-sm-'.$form_input_width}}">
        <select style='width:100%' class='form-control select2' id="{{$name}}"
                {{$required}} {{$readonly}} {!!$placeholder!!} {{$disabled}} name="{{$name}}{{($form['relationship_table'])?'[]':''}}" {{ ($form['relationship_table'])?'multiple="multiple"':'' }} >
            <option value=''>{{trans('crudbooster.text_prefix_option')}} {{$form['label']}}</option>
            @if($form['dataenum'])
                <?php
                $dataenum = $form['dataenum'];
                $dataenum = (is_array($dataenum)) ? $dataenum : explode(";", $dataenum);
                ?>
                @foreach($dataenum as $enum)
                    <?php
                    $val = $lab = '';
                    if (strpos($enum, '|') !== FALSE) {
                        $draw = explode("|", $enum);
                        $val = $draw[0];
                        $lab = $draw[1];
                    } else {
                        $val = $lab = $enum;
                    }

                    $select = ($value == $val) ? "selected" : "";
                    ?>
                    <option {{$select}} value='{{$val}}'>{{$lab}}</option>
                @endforeach
            @endif

            @if($form['datatable'])
                @if($form['relationship_table'])
                    <?php
                    $select_table = explode(',', $form['datatable'])[0];
                    $select_title = explode(',', $form['datatable'])[1];
                    $select_where = $form['datatable_where'];
                    $pk = CRUDBooster::findPrimaryKey($select_table);

                    $result = DB::table($select_table)->select($pk, $select_title);
                    if ($select_where) {
                        $result->whereraw($select_where);
                    }
                    $result = $result->orderby($select_title, 'asc')->get();


                    $foreignKey = CRUDBooster::getForeignKey($table, $form['relationship_table']);
                    $foreignKey2 = CRUDBooster::getForeignKey($select_table, $form['relationship_table']);

                    $value = DB::table($form['relationship_table'])->where($foreignKey, $id);
                    $value = $value->pluck($foreignKey2)->toArray();

                    foreach ($result as $r) {
                        $option_label = $r->{$select_title};
                        $option_value = $r->id;
                        $selected = (is_array($value) && in_array($r->$pk, $value)) ? "selected" : "";
                        echo "<option $selected value='$option_value'>$option_label</option>";
                    }
                    ?>
                @else
                    @if($form['datatable_ajax'] == false)
                            <?php
                            $select_table = explode(',', $form['datatable'])[0];
                            $select_title = explode(',', $form['datatable'])[1];
                            $select_where = $form['datatable_where'];
                            $datatable_format = $form['datatable_format']?:"[".$select_title."]";
                            $select_table_pk = CRUDBooster::findPrimaryKey($select_table);
                            $result = DB::table($select_table);
                            if ($select_where) {
                                $result->whereraw($select_where);
                            }
                            if (CRUDBooster::isColumnExists($select_table, 'deleted_at')) {
                                $result->whereNull('deleted_at');
                            }
                            $result = $result->get();
                            $resultFinal = [];
                            foreach($result as $r)
                            {
                                $display = $datatable_format;
                                foreach($r as $a=>$b) {
                                    $display = str_replace("[".$a."]",$b, $display);
                                }
                                $resultFinal[] = ['id'=>$r->$select_table_pk, 'display'=>$display];
                            }

                            $resultFinal = collect($resultFinal)->sortBy('display')->all();

                            foreach ($resultFinal as $r) {
                                $selected = ($r['id'] == $value) ? "selected" : "";
                                echo "<option $selected value='".$r['id']."'>".$r['display']."</option>";
                            }
                            ?>
                    <!--end-datatable-ajax-->
                    @endif

                <!--end-relationship-table-->
                @endif

            <!--end-datatable-->
            @endif
        </select>
        <div class="text-danger">
            {!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}
        </div><!--end-text-danger-->
        <p class='help-block'>{{ @$form['help'] }}</p>
    </div>
</div>
