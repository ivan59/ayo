<div class='form-group {{$header_group_class}} {{ ($errors->first($name))?"has-error":"" }}' id='form-group-{{$name}}' style="{{@$form['style']}}">
    <label class='control-label col-sm-{{$form_label_width }}'>{{$form['label']}}
        @if($required)
            <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
        @endif
    </label>
    <div class="{{$col_width?:'col-sm-'.$form_input_width}}">

        <!--Input File-->
        <div class="input-file" style="display: {{ ($value)?'none':'block' }}">
            <input type='file' id="{{$name}}" data-required="{{$required?1:0}}" title="{{$form['label']}}" {{ (!$value)?$required:"" }} {{$readonly}} {{$disabled}} class='form-control' name="{{$name}}"/>
            <input type="hidden" name="_{{$name}}" value="{{$value}}">
            <p class='help-block'>{{ @$form['help'] }}</p>
        </div>

        <div class="input-file-value" style="display: {{ ($value)?'block':'none' }};">
        @if($value)
            <?php
            if(Storage::exists($value) || file_exists($value)):
            $url = asset($value);
            $ext = pathinfo($url, PATHINFO_EXTENSION);
            $images_type = array('jpg', 'png', 'gif', 'jpeg', 'bmp', 'tiff');
            if(in_array(strtolower($ext), $images_type)):
            ?>
            <p><a data-lightbox='roadtrip' href='{{$url}}'><img style='max-width:160px' title="Image For {{$form['label']}}" src='{{$url}}'/></a></p>
            <?php else:?>
            <p><a href='{{$url}}'>{{trans("crudbooster.button_download_file")}}</a></p>
            <?php endif;
            else:
                echo "<p class='text-danger'><i class='fa fa-exclamation-triangle'></i> ".trans("crudbooster.file_broken")."</p>";
            endif;
            ?>
                <p><a class='btn btn-danger btn-delete btn-sm' data-confirm-text="{{trans("crudbooster.delete_title_confirm")}}"
                      onclick="removeFile(this);"
                      href='javascript:;'><i
                                class='fa fa-ban'></i> {{trans('crudbooster.text_delete')}} </a>
                </p>

                <p class='text-muted'><em>{{trans("crudbooster.notice_delete_file_upload")}}</em></p>
        @endif
        </div><!--end-input-file-value-->
        
        <div class="text-danger">{!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}</div>
    </div>
</div>
<script>

</script>
