<script>
    function removeFile(t) {

        let confirmText = $(t).data('confirm-text');

        if(confirm(confirmText)) {
            let formGroup = $(t).parents('.form-group');
            //hide this box
            formGroup.find('.input-file-value').hide();

            //show input box
            formGroup.find('.input-file').show();

            let isRequred = formGroup.find('.input-file input[type=file]').data('required');
            if(isRequred==1) {
                formGroup.find('.input-file input[type=file]').prop('required',true);
            }else{
                formGroup.find('.input-file input[type=file]').prop('required',false);
            }

            formGroup.find('.input-file input[type=hidden]').val(null);
        }
    }
</script>