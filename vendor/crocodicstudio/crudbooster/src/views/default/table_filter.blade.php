<script>
    $(function () {
        $('#btn_advanced_filter').click(function () {
            $('#advanced_filter_modal').modal('show');
        })
    })
</script>
<!-- MODAL FOR SORTING DATA-->
<div class="modal fade" tabindex="-1" role="dialog" id='advanced_filter_modal'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" type="button" data-dismiss="modal">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><i class='fa fa-filter'></i> {{trans("crudbooster.filter_dialog_title")}}</h4>
            </div>
            <form method='get' action=''>
                <div class="modal-body">

                    @if(!$filter_view)
                        <?php foreach($columns as $key => $col):?>
                        <?php if ($col['visible'] === FALSE || $col['filter_by']===false) continue;?>

                        <div class='form-group'>

                            <div class='row-filter-combo row'>

                                <div class="col-sm-4">
                                    <strong>{{$col['label']}}</strong>
                                </div>

                                <div class="col-sm-8">

                                    @if($col['filter_by'] == "date")
                                        <input autocomplete="off" type="text" style="width: 150px; display: inline-block" class="form-control datepicker" value="{{g('filter_column')[$col['name']]['start']}}" name="filter_column[{{$col['name']}}][start]"> to
                                        <input autocomplete="off" type="text" style="width: 150px; display: inline-block" class="form-control datepicker" value="{{g('filter_column')[$col['name']]['end']}}" name="filter_column[{{$col['name']}}][end]">
                                    @elseif($col['filter_by'] == "int" || $col['filter_by'] == 'range' || $col['filter_by'] == 'number')
                                        <input autocomplete="off" type="number" style="width: 150px; display: inline-block" class="form-control" value="{{g('filter_column')[$col['name']]['start']}}" name="filter_column[{{$col['name']}}][start]"> to
                                        <input autocomplete="off" type="number" style="width: 150px; display: inline-block" class="form-control" value="{{g('filter_column')[$col['name']]['end']}}" name="filter_column[{{$col['name']}}][end]">
                                    @elseif($col['filter_by'] == 'exists' || $col['filter_by'] == 'exist')
                                        <select name="filter_column[{{$col['name']}}]" class="form-control select2">
                                            <option value="">** Select a {{$col['label']}}</option>
                                            <?php
                                                $groupBy = $resultQuery->groupBy($col['alias'])->pluck($col['alias'])->toArray();
                                            ?>
                                            @foreach($groupBy as $group)
                                                @php $select = g('filter_column')[$col['name']]==$group?'selected':''; @endphp
                                                <option {{ $select }} value="{{ $group }}">{{ $group }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <input type="text" class="form-control" value="{{g('filter_column')[$col['name']]}}" name="filter_column[{{$col['name']}}]">
                                    @endif

                                </div><!--end col-sm-10-->
                            </div><!--end filter combo-->

                        </div>
                        <?php endforeach;?>
                    @else
                        {!! $filter_view !!}
                    @endif
                </div>
                <div class="modal-footer" align="right">
                    <button class="btn btn-default" type="button" data-dismiss="modal">{{trans("crudbooster.button_close")}}</button>
                    <button class="btn btn-default btn-reset" type="reset"
                            onclick='location.href="{{Request::get("lasturl")}}"'>{{trans("crudbooster.button_reset")}}</button>
                    <button class="btn btn-primary btn-submit" type="submit">{{trans("crudbooster.button_submit")}}</button>
                </div>
                {!! CRUDBooster::getUrlParameters(['filter_column','lasturl']) !!}
                <input type="hidden" name="lasturl" value="{{Request::get('lasturl')?:Request::fullUrl()}}">
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>