@if(count($result)==0)
    <tr class='warning'>
        <?php if($button_bulk_action && $show_numbering):?>
        <td colspan='{{count($columns)+3}}' align="center">
        <?php elseif( ($button_bulk_action && ! $show_numbering) || (! $button_bulk_action && $show_numbering) ):?>
        <td colspan='{{count($columns)+2}}' align="center">
        <?php else:?>
        <td colspan='{{count($columns)+1}}' align="center">
            <?php endif;?>

            <i class='fa fa-search'></i> {{trans("crudbooster.table_data_not_found")}}
        </td>
    </tr>
@endif

@php
    $page = (Request::get('page')) ? Request::get('page') : 1;
    $number = ($page - 1) * $limit + 1;
@endphp
@foreach($result as $row)
    <tr>
        <!-- checkbox input -->
        @if($button_bulk_action && !g('fileformat'))
        <td><input type="checkbox" class="checkbox" name="checkbox[]" value="{{ $row->$table_pk }}"></td>
        @endif

        @if($show_numbering)
        <td>{{ $number++ }}</td>
        @endif

        <!-- columns loop -->
        @foreach($columns as $column)
            <?php
                if (isset($column['visible']) && $column['visible'] === FALSE) {
                    continue;
                }
                $value = $row->{$column['alias']};
                $textAlign = (isset($column['align']))?"text-align:".$column['align']:"text-align:left";
                $prefix = (isset($column['prefix']))?$column['prefix']:null;
                $suffix = (isset($column['suffix']))?$column['suffix']:null;
            ?>

            @if(isset($column['image']) && $column['image']===TRUE)
                @php $value = ($value)?:'vendor/crudbooster/assets/adminlte/dist/img/default-50x50.gif'; @endphp
                <td style="{{ $textAlign }};" data-image="1">{!! $prefix !!}<a data-lightbox="roadtrip" href="{{ asset($value) }}"><img src="{{ asset($value) }}?w=50&h=50" width="50px" height="50px" class="img-rounded" alt="thumbnail"></a>{!! $suffix !!}</td>
            @elseif(isset($column['download']) && $column['download']===TRUE)
                <td style="{{ $textAlign }};" data-download="1">{!! $prefix !!}<a href="{{ asset($value) }}" target="_blank"><i class="fa fa-file"></i> {{ basename($value) }}</a>{!! $suffix !!}</td>
            @elseif(isset($column['callback']))
                @php
                    $callbackValue = call_user_func($column['callback'], $row);
                    if(g('fileformat')) $callbackValue = strip_tags($callbackValue,"<br><div><p><span>");
                @endphp
                <td style="{{ $textAlign }};" data-callback="1">{!! $prefix !!}{!! $callbackValue !!}{!! $suffix !!}</td>
            @elseif(isset($column['str_limit']))
                <td style="{{ $textAlign }};" data-limit="1">{!! $prefix !!} {{ str_limit(strip_tags($value), $column['str_limit']) }}{!! $suffix !!}</td>
            @elseif(isset($column['nl2br']) && $column['nl2br'] === TRUE)
                <td style="{{ $textAlign }};" data-nl2br="1">{!! $prefix !!}{!! nl2br(strip_tags($value)) !!}{!! $suffix !!}</td>
            @elseif(isset($column['date_format']))
                <td style="{{ $textAlign }};" data-dateformat="1">{!! $prefix !!}{{ date($column['date_format'], strtotime($value)) }}{!! $suffix !!}</td>
            @else
                <td style="{{ $textAlign }};" data-default="1">{!! $prefix !!}{{ $value }}{!! $suffix !!}</td>
            @endif
        @endforeach

        <!-- action td -->
        @if($button_table_action && !g('fileformat'))
        <td><div align="right">
            @include('crudbooster::default.partials.action')
            </div>
        </td>
        @endif
    </tr>
@endforeach