@extends('crudbooster::admin_template')
@section('content')


    <div class="row">
        @if($compact_form === true && $compact_form_align == "center")
            <div class="col-sm-2"></div>
        @endif
        @if($compact_form === true && $compact_form_align == "right")
            <div class="col-sm-4"></div>
        @endif
        <div class="col-sm-{{ $compact_form===true?8:12 }}">
            @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
                @if(g('return_url'))
                    <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                            &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
                @else
                    <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                            &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
                @endif
            @endif

            @if($command == 'detail')
                @foreach($formsGroup as $group => $form)
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h1 class="box-title">{{ $group }}</h1>
                        </div>

                        <div class="box-body">
                            <?php $forms = $form; ?>
                            @include("crudbooster::default.form_detail")
                        </div>
                    </div>
                @endforeach
            @else
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h1 class="box-title">{{ $page_title }}</h1>
                    </div>

                    <?php
                    $action = (@$row) ? CRUDBooster::mainpath("edit-save/$row->id") : CRUDBooster::mainpath("add-save");
                    $return_url = ($return_url) ?: g('return_url');
                    ?>
                    <form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action='{{$action}}'>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                        <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                        <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                        @if($hide_form)
                            <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                        @endif
                    <div class="box-body">
                        <div id="parent-form-area">
                            @include("crudbooster::default.form_body")
                        </div><!-- /.box-body -->
                    </div>
                    @if(CRUDBooster::getCurrentMethod()!='getDetail')
                        <div class="box-footer">

                            <div class="form-group">
                                <div class="col-sm-12" align="right">
                                    @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                        @if(g('return_url'))
                                            <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                        class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                        @else
                                            <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                        class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                        @endif
                                    @endif

                                    @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                        @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                            <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-primary'>
                                        @endif

                                        @if($button_save && $command != 'detail')
                                            <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success'>
                                        @endif

                                    @endif
                                </div>
                            </div>


                        </div><!-- /.box-footer-->
                    @endif
                    </form>
                </div>

            @endif
        </div><!--END AUTO MARGIN-->
        @if($compact_form === true && $compact_form_align == "center")
            <div class="col-sm-2"></div>
        @endif
        @if($compact_form === true && $compact_form_align == "left")
            <div class="col-sm-4"></div>
        @endif
    </div><!--end row-->

    <!-- view after detail-->
    @if($command == 'detail' && isset($viewAfterDetail))
        {!! view($viewAfterDetail, $viewAfterDetailData)->render() !!}
    @endif

@endsection