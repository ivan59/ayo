@push('bottom')
    <script type="text/javascript">
        $(document).ready(function () {
            var $window = $(window);

            function checkWidth() {
                var windowsize = $window.width();
                if (windowsize > 500) {
                    console.log(windowsize);
                    $('#box-body-table').removeClass('table-responsive');
                } else {
                    console.log(windowsize);
                    $('#box-body-table').addClass('table-responsive');
                }
            }

            checkWidth();
            $(window).resize(checkWidth);
        });
    </script>
@endpush

<form id='form-table' method='post' action='{{CRUDBooster::mainpath("action-selected")}}'>
    <input type='hidden' name='button_name' value=''/>
    {!! csrf_field() !!}
    <table id='table_dashboard' class="table table-hover table-striped table-bordered" style="margin-bottom: 0px; margin-top: 0px">
        <thead>
            @include('crudbooster::default.table_thead')
        </thead>
        <tbody>
            @include('crudbooster::default.table_tbody')
        </tbody>
        <tfoot>
        <tr>
            <?php if($button_bulk_action):?>
            <th>&nbsp;</th>
            <?php endif;?>

            <?php if($show_numbering):?>
            <th>&nbsp;</th>
            <?php endif;?>

            <?php
            foreach ($columns as $col) {
                if ($col['visible'] === FALSE) continue;
                $colname = $col['label'];
                $width = ($col['width']) ?: "auto";
                echo "<th width='$width'>$colname</th>";
            }
            ?>

            @if($button_table_action)
                @if(CRUDBooster::isUpdate() || CRUDBooster::isDelete() || CRUDBooster::isRead())
                    <th> </th>
                @endif
            @endif
        </tr>
        </tfoot>
    </table>

</form><!--END FORM TABLE-->


@if($columns)
    @push('bottom')
        <script>
            $(function () {

                $("#table_dashboard .checkbox").click(function () {
                    var is_any_checked = $("#table_dashboard .checkbox:checked").length;
                    if (is_any_checked) {
                        $(".btn-delete-selected").removeClass("disabled");
                    } else {
                        $(".btn-delete-selected").addClass("disabled");
                    }
                })

                $("#table_dashboard #checkall").click(function () {
                    var is_checked = $(this).is(":checked");
                    $("#table_dashboard .checkbox").prop("checked", !is_checked).trigger("click");
                })

                $('.selected-action ul li a').click(function () {
                    var name = $(this).data('name');
                    $('#form-table input[name="button_name"]').val(name);
                    var title = $(this).attr('title');

                    swal({
                            title: "{{trans("crudbooster.confirmation_title")}}",
                            text: "{{trans("crudbooster.alert_bulk_action_button")}} " + title + " ?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#008D4C",
                            confirmButtonText: "{{trans('crudbooster.confirmation_yes')}}",
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true
                        },
                        function () {
                            $('#form-table').submit();
                        });

                })

                $('table tbody tr .button_action a').click(function (e) {
                    e.stopPropagation();
                })
            })
        </script>

        @include('crudbooster::default.table_filter')


        @include('crudbooster::default.table_export_dialog')
    @endpush
@endif
