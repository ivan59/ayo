<table border='1' {!! Request::input('fileformat')=='pdf'?"width='100%'":"" !!}  cellpadding='3' cellspacing="0" style='border-collapse: collapse;font-size:12px'>

    <thead>
        @include('crudbooster::default.table_thead')
    </thead>
    <tbody>
        @include('crudbooster::default.table_tbody')
    </tbody>
</table>