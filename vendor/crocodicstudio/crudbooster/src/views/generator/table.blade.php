
@if(isset($back_url))
    <p>
        <a href="{{ $back_url }}" class="btn btn-default">&laquo; Back to previous page</a>
    </p>
@endif

<div class="box box-default">
    <div class="box-header">
        <h1 class="box-title">{{ $title }}</h1>
        <div class="box-tools pull-right">
            <a href="javascript:;" onclick="add{{$uniqueID}}()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New Data</a>
        </div>
    </div>
    <div class="box-body">
        <table class="table datatables-simple">
            <thead>
                @include('crudbooster::generator.thead')
            </thead>
            <tbody>
                @include("crudbooster::generator.tbody")
            </tbody>
        </table>
    </div>
</div>

@include('crudbooster::generator.script')