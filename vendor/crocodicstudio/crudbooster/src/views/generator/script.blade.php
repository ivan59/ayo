
<script>
    function add{{$uniqueID}}() {
        var modal = $('#modal-form-{{$uniqueID}}');
        modal.modal('show');
        modal.find('.modal-title').text('Add New Data');
        modal.find('input[name=_action]').val('addSave');
        modal.find('.modal-footer button[type=submit]').text('Add');
        modal.find('.modal-footer button[type=button]').text('Close');
        modal.find('.modal-body').html("<h3 align='center'><i class='fa fa-spin fa-spinner'></i></h3>");
        $.post("{{ $url }}",{
            _token:"{{csrf_token()}}",
            _action:'add',
            _generator:'{{$uniqueID}}'},function (response) {
            modal.find('.modal-body').html(response);
            modal.find('.modal-body .control-label').removeClass('col-sm-2').addClass('col-sm-3');
        });
    }

    function edit{{$uniqueID}}(id) {
        var modal = $('#modal-form-{{$uniqueID}}');
        modal.modal('show');
        modal.find('.modal-title').text('Edit Data');
        modal.find('input[name=_action]').val('editSave');
        modal.find('.modal-footer button[type=submit]').text('Update');
        modal.find('.modal-footer button[type=button]').text('Close');
        modal.find('form input[name=id]').val(id);
        modal.find('.modal-body').html("<h3 align='center'><i class='fa fa-spin fa-spinner'></i></h3>");
        $.post("{{ $url }}",{
            _token:"{{csrf_token()}}",
            _action:'edit',_generator:'{{$uniqueID}}',
            id: id
        },function (response) {
            modal.find('.modal-body').html(response);
            modal.find('.modal-body .control-label').removeClass('col-sm-2').addClass('col-sm-3');
        });
    }

    function remove{{$uniqueID}}(id) {
        var modal = $('#modal-form-{{$uniqueID}}');
        modal.modal('show');
        modal.find('.modla-title').text('Delete Data');
        modal.find('input[name=_action]').val('delete');
        modal.find('.modal-footer button[type=submit]').text('Yes');
        modal.find('.modal-footer button[type=button]').text('No');
        modal.find(".modal-body").html("<h3 align='center'>Are you sure want to delete?</h3>");
    }
</script>

<div class="modal" id="modal-form-{{$uniqueID}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Form</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{ $url }}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_generator" value="{{ $uniqueID }}">
                <input type="hidden" name="_action" value="add">
                <input type="hidden" name="id" value="">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->