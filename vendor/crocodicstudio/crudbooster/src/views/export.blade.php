@if(Request::input('fileformat') == 'pdf')
    <h3>{{Request::input('filename')}}</h3>
@endif

<table border='1' {!! Request::input('fileformat')=='pdf'?"width='100%'":"" !!}  cellpadding='3' cellspacing="0" style='border-collapse: collapse;font-size:12px'>

    <thead style="font-weight:bold;">
        @include('crudbooster::default.table_thead')
    </thead>
    <tbody>
        @include('crudbooster::default.table_tbody')
    </tbody>
</table>
<script type="text/php">
    if ( isset($pdf) ) {
        $font = Font_Metrics::get_font("helvetica", "bold");
        $pdf->page_text(36, 18, "Page {PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
    }
</script>