<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\scaffold\Columns as Col;
use crocodicstudio\crudbooster\controllers\CBController;

class AdminCmsUsersController extends CBController {


    public function cbInit() {

        $this->table               = 'cms_users';
        $this->title_field         = "name";
        $this->button_action_style = 'button_icon';
        $this->button_import 	   = FALSE;
        $this->button_export 	   = FALSE;
        $this->compact_form        = true;
        $this->form_input_width = 9;
        $this->form_label_width = 3;

        Col::add("Name","name");
        Col::add("Email","email");
        Col::add("Privilege","cms_privileges_name")->fetchFrom("cms_privileges","id_cms_privileges");
        Col::add("Photo","photo")->imageable();

        $this->form = array();
        $this->form[] = array("label"=>"Name","name"=>"name",'required'=>true,'validation'=>'required|alpha_spaces|min:3');
        $this->form[] = array("label"=>"Email","name"=>"email",'required'=>true,'type'=>'email','validation'=>'required|email|unique:cms_users,email,'.CB::getCurrentId());
        $this->form[] = array("label"=>"Photo","name"=>"photo","type"=>"upload","help"=>"Recommended resolution is 200x200px",'required'=>true,'validation'=>'required|image|max:1000','resize_width'=>90,'resize_height'=>90);
        $this->form[] = array("label"=>"Privilege","name"=>"id_cms_privileges","type"=>"select","datatable"=>"cms_privileges,name",'required'=>true);
        $this->form[] = array("label"=>"Password","name"=>"password","type"=>"password","help"=>"Please leave empty if not change");

    }

    public function getProfile() {

        $this->button_addmore = FALSE;
        $this->button_cancel  = FALSE;
        $this->button_show    = FALSE;
        $this->button_add     = FALSE;
        $this->button_delete  = FALSE;
        $this->hide_form 	  = ['id_cms_privileges'];

        $row = CB::first('cms_users',CB::myId());
        return $this->viewEdit(['row'=>$row,'page_title'=>trans("crudbooster.label_button_profile")]);
    }
}
