<?php 
namespace App\Http\Controllers;

use DB;
use Session;
use Request;

class CBHook extends Controller {

	/*
	| --------------------------------------
	| Please note that you should re-login to see the session work
	| --------------------------------------
	|
	*/
	public static function afterLogin() {
		
	}

    public static function beforeDashboard()
    {

    }

	public static function beforeBackendMiddleware()
    {
        
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public static function beforeAuthAPIMiddleware($request)
    {

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Response $response
     * @param $duration
     */
    public static function afterAuthAPIMiddleware($request, $response, $duration)
    {

    }
}