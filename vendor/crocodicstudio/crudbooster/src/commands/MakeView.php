<?php namespace crocodicstudio\crudbooster\commands;

use App;
use Illuminate\Console\Command;

class MakeView extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crudbooster:make:view {filename} {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CRUDBooster make view';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('filename');
        $type = $this->argument('type')?:'blank';
        $path = resource_path('views');
        $filenames = explode('.',$filename);
        $filenamesCount = count($filenames);

        for($i=0;$i<$filenamesCount;$i++) {
            if($i == ($filenamesCount-1)) {
                $template = file_get_contents(__DIR__.'/../views/stubs/'.$type.'.blade.php.stub');
                file_put_contents($path.'/'.$filenames[$i].'.blade.php', $template);
            }else{
                @mkdir($path.'/'.$filenames[$i]);
                $path = $path.'/'.$filenames[$i];
            }
        }

        $this->comment('View '.$filename.' has been created!');
    }
}
