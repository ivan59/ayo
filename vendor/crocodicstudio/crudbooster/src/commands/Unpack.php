<?php namespace crocodicstudio\crudbooster\commands;

use App;
use Illuminate\Console\Command;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Artisan;
use Schema;

class Unpack extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'crudbooster:unpack';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CRUDBooster unpacking the project';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("CRUDBooster Unpacking");
        Schema::disableForeignKeyConstraints();
        $tables = CRUDBooster::listTables();
        foreach($tables as $table) {
            $table_array = get_object_vars($table);
            Schema::drop($table_array[key($table_array)]);
        }
        $this->info('Migrating the tables...');
        $this->call("migrate");

        $this->info('Seeding the data...');
        $this->call("db:seed",["--class"=>"DefaultData"]);
        Schema::enableForeignKeyConstraints();
        $this->info("Unpacking completed!");
    }
}
