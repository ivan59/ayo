<?php namespace crocodicstudio\crudbooster\controllers;

error_reporting(E_ALL ^ E_NOTICE);

use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\helpers\Columns;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\PDF;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Schema;

class CBController extends Controller
{
    public $data_inputan;

    public $columns_table;

    public $module_name;

    public $table;

    public $title_field;

    public $primary_key = 'id';

    public $arr = [];

    public $col = [];

    public $form = [];

    public $data = [];

    public $addaction = [];

    public $orderby = null;

    public $password_candidate = null;

    public $date_candidate = null;

    public $limit = 20;

    public $global_privilege = false;

    public $show_numbering = false;

    public $alert = [];

    public $index_button = [];

    public $button_filter = true;

    public $button_export = true;

    public $button_import = true;

    public $button_show = true;

    public $button_addmore = true;

    public $button_table_action = true;

    public $button_bulk_action = true;

    public $button_add = true;

    public $button_delete = true;

    public $button_cancel = true;

    public $button_save = true;

    public $button_edit = true;

    public $button_detail = true;

    public $button_action_style = 'button_icon';

    public $button_action_width = null;

    public $index_statistic = [];

    public $index_additional_view = [];

    public $pre_index_html = null;

    public $post_index_html = null;

    public $load_js = [];

    public $load_css = [];

    public $script_js = null;

    public $style_css = null;

    public $sub_module = [];

    public $show_addaction = true;

    public $table_row_color = [];
    public $button_selected = [];
    public $return_url = null;
    public $parent_field = null;
    public $parent_id = null;
    public $hide_form = [];
    public $index_return = false; //for export
    public $sidebar_mode = 'normal';

    private $viewAfterDetail = null;
    private $viewAfterDetailData = [];
    public $auto_modal_form = true;
    public $modal_form = false;
    public $form_label_width = 2;
    public $form_input_width = 9;
    public $filter_view = null;
    private $current_row = null;

    private $hideReadButtonCondition = null;
    private $hideEditButtonCondition = null;
    private $hideDeleteButtonCondition = null;

    private $pageTitle = null;
    private $pageDesc = null;

    public $compact_form = false;
    public $compact_form_align = 'center';

    public function cbInit()
    {

    }

    public function hideReadButtonCondition($callback)
    {
        $this->hideReadButtonCondition = $callback;
    }

    public function hideEditButtonCondition($callback)
    {
        $this->hideEditButtonCondition = $callback;
    }

    public function hideDeleteButtonCondition($callback)
    {
        $this->hideDeleteButtonCondition = $callback;
    }

    public function assignCols() {
        $data = $this->col;

        foreach($data as &$d) {
            $d['alias'] = str_replace('.','_',$d['name']);
        }

        $columns = app('crudbooster_columns')->getColumn();
        if(isset($columns)) {
            foreach($columns as $name=>$col) {
                $data[] = $col;
            }
        }

        foreach($data as &$d) {
            if(!isset($d['filter_by']) && $type = CB::getFieldType($this->table, $d['name'])) {
                if($type == 'varchar') {
                    $d['filter_by'] = 'text';
                }else{
                    $d['filter_by'] = $type;
                }
            }
        }

        return $data;
    }

    public function cbLoader()
    {
        $this->cbInit();

        $this->checkHideForm();
        $this->primary_key = CB::pk($this->table);
        $this->columns_table = $this->assignCols();
        $this->addaction = app("crudbooster_columns")->getAddAction();

        if ($this->sub_module) {
            foreach ($this->sub_module as $s) {
                $table_parent = CRUDBooster::parseSqlTable($this->table)['table'];
                $this->addaction[] = [
                    'label' => $s['label'],
                    'icon' => $s['button_icon'],
                    'url' => CRUDBooster::adminPath($s['path']).'?parent_table='.$table_parent.'&parent_columns='.$s['parent_columns'].'&parent_columns_alias='.$s['parent_columns_alias'].'&parent_id=['.(! isset($s['custom_parent_id']) ? "id" : $s['custom_parent_id']).']&return_url='.urlencode(Request::fullUrl()).'&foreign_key='.$s['foreign_key'].'&label='.urlencode($s['label']),
                    'color' => $s['button_color'],
                    'showIf' => $s['showIf'],
                ];
            }
        }

        $this->data_inputan = $this->form;
        $this->data['pk'] = $this->primary_key;
        $this->data['forms'] = $this->data_inputan;
        $this->data['formsGroup'] = $this->groupingForm($this->data_inputan);
        $this->data['hide_form'] = $this->hide_form;
        $this->data['addaction'] = ($this->show_addaction) ? $this->addaction : null;
        $this->data['table'] = $this->table;
        $this->data['title_field'] = $this->title_field;
        $this->data['appname'] = CRUDBooster::getSetting('appname');
        $this->data['alerts'] = $this->alert;
        $this->data['hide_read_button_condition'] = $this->hideReadButtonCondition;
        $this->data['hide_edit_button_condition'] = $this->hideEditButtonCondition;
        $this->data['hide_delete_button_condition'] = $this->hideDeleteButtonCondition;
        $this->data['index_button'] = $this->index_button;
        $this->data['show_numbering'] = $this->show_numbering;
        $this->data['button_detail'] = $this->button_detail;
        $this->data['button_edit'] = $this->button_edit;
        $this->data['button_show'] = $this->button_show;
        $this->data['button_add'] = $this->button_add;
        $this->data['button_delete'] = $this->button_delete;
        $this->data['button_filter'] = $this->button_filter;
        $this->data['button_export'] = $this->button_export;
        $this->data['button_addmore'] = $this->button_addmore;
        $this->data['button_cancel'] = $this->button_cancel;
        $this->data['button_save'] = $this->button_save;
        $this->data['button_table_action'] = $this->button_table_action;
        $this->data['button_bulk_action'] = $this->button_bulk_action;
        $this->data['button_import'] = $this->button_import;
        $this->data['button_action_width'] = $this->button_action_width;
        $this->data['button_selected'] = $this->button_selected;
        $this->data['index_statistic'] = $this->index_statistic;
        $this->data['index_additional_view'] = $this->index_additional_view;
        $this->data['table_row_color'] = $this->table_row_color;
        $this->data['pre_index_html'] = $this->pre_index_html;
        $this->data['post_index_html'] = $this->post_index_html;
        $this->data['load_js'] = $this->load_js;
        $this->data['load_css'] = $this->load_css;
        $this->data['script_js'] = $this->script_js;
        $this->data['style_css'] = $this->style_css;
        $this->data['sub_module'] = $this->sub_module;
        $this->data['parent_field'] = (g('parent_field')) ?: $this->parent_field;
        $this->data['parent_id'] = (g('parent_id')) ?: $this->parent_id;
        $this->data['auto_modal_form'] = $this->auto_modal_form;
        $this->data['modal_form'] = $this->modal_form;
        $this->data['form_label_width'] = $this->form_label_width;
        $this->data['form_input_width'] = $this->form_input_width;
        $this->data['filter_view'] = $this->filter_view;
        $this->data['compact_form'] = $this->compact_form;
        $this->data['compact_form_align'] =  $this->compact_form_align;

        if ($this->sidebar_mode == 'mini') {
            $this->data['sidebar_mode'] = 'sidebar-mini';
        } elseif ($this->sidebar_mode == 'collapse') {
            $this->data['sidebar_mode'] = 'sidebar-collapse';
        } elseif ($this->sidebar_mode == 'collapse-mini') {
            $this->data['sidebar_mode'] = 'sidebar-collapse sidebar-mini';
        } else {
            $this->data['sidebar_mode'] = '';
        }

        if (CRUDBooster::getCurrentMethod() == 'getProfile') {
            Session::put('current_row_id', CRUDBooster::myId());
            $this->data['return_url'] = Request::fullUrl();
        }

        view()->share($this->data);
    }

    public function currentRow()
    {
        return $this->current_row;
    }

    public function currentRowId()
    {
        return Session::get('current_row_id');
    }

    private function groupingForm($forms)
    {
        $result = [];
        $lastKey = 'Detail Data';
        foreach($forms as $form)
        {
            if($form['type']=='header')
            {
                $lastKey = $form['label'];
            }
            $result[$lastKey][] = $form;
        }
        return $result;
    }

    public function setPageTitle($title) {
        $this->pageTitle = $title;
    }

    public function setPageDescription($desc) {
        $this->pageDesc = $desc;
    }

    public function viewEdit($data) {
        $this->cbLoader();
        $data['command'] = 'edit';
        return view('crudbooster::default.form', $data);
    }

    public function viewAdd($data) {
        $this->cbLoader();
        $data['command'] = 'add';
        return view('crudbooster::default.form', $data);
    }

    public function viewDetail($data) {
        $this->cbLoader();
        $data['command'] = 'detail';
        return view('crudbooster::default.form', $data);
    }

    private function checkHideForm()
    {
        if ($this->hide_form && count($this->hide_form)) {
            foreach ($this->form as $i => $f) {
                if (in_array($f['name'], $this->hide_form)) {
                    unset($this->form[$i]);
                }
            }
        }

        if(request()->segment(3)=='edit' || request()->segment(3)=='edit-save')
        {
            foreach ($this->form as $i => $f) {
                $name = $f['name'];
                if(isset($f['hide_from_edit']) && $f['hide_from_edit']==true) {
                    unset($this->form[$i]);
                }
            }
        }

        if(request()->segment(3)=='edit' || request()->segment(3)=='add-save')
        {
            foreach ($this->form as $i => $f) {
                $name = $f['name'];
                if(isset($f['hide_from_add']) && $f['hide_from_add']==true) {
                    unset($this->form[$i]);
                }
            }
        }

        if(request()->segment(3)=='detail')
        {
            foreach ($this->form as $i => $f) {
                $name = $f['name'];
                if(isset($f['hide_from_detail']) && $f['hide_from_detail']==true) {
                    unset($this->form[$i]);
                }
            }
        }
    }

    public function getIndex()
    {
        $this->cbLoader();

        $module = CRUDBooster::getCurrentModule();

        if (! CRUDBooster::isView() && $this->global_privilege == false) {
            CRUDBooster::insertLog(trans('crudbooster.log_try_view', ['module' => $module->name]));
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
        }

        $data['table'] = $this->table;
        $data['table_pk'] = CB::pk($this->table);
        $data['page_title'] = $module->name;
        $data['page_description'] = trans('crudbooster.default_module_description');
        $data['date_candidate'] = $this->date_candidate;
        $data['limit'] = $limit = (Request::get('limit')) ? Request::get('limit') : $this->limit;
        $tablePK = $data['table_pk'];

        $data['columns'] = $this->columns_table;


        $result = new CBBaseQuery($this->columns_table, $this->table);

        $resultQuery = $result->getInstanceQuery();

        $this->hook_query_index($resultQuery);
        $data['resultQuery'] = $resultQuery;

        $result->setInstanceQuery($resultQuery);

        if(request()->has('q')) {
            $result->find(request('q'));
        }

        if(request('filter_column')) {
            foreach(request('filter_column') as $key => $val) {
                if(is_array($val)) {
                    if($val['start'] || $val['end']) {
                        $start = $end = null;
                        if($val['start'] && !$val['end']) {
                            $start = $val['start'];
                        }elseif ($val['end'] && !$val['start']) {
                            $end = $val['end'];
                        }elseif($val['start'] && $val['end']) {
                            $start = $val['start'];
                            $end = $val['end'];
                        }
                        $result->between($key, $start, $end);
                    }
                }else{
                    $result->like($key, $val);
                }
            }
        }

        if(request('orderby') && request('orderby_val')) {
            $result->orderBy(request('orderby'), request('orderby_val'));
        }elseif($this->orderby) {
            $result->orderBy($this->orderby[0], $this->orderby[1]);
        }

        if($this->index_return) {
            $data['result'] = $result->get();
            return $data;
        }

        $result = $result->paginate($limit);

        $data['result'] = $result;

        return view("crudbooster::default.index", $data);
    }

    public function getExportData()
    {

        return redirect(CRUDBooster::mainpath());
    }

    public function postExportData()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(180);

        $this->limit = Request::input('limit');
        $this->index_return = true;
        $filetype = Request::input('fileformat');
        $filename = Request::input('filename');
        $papersize = Request::input('page_size');
        $paperorientation = Request::input('page_orientation');
        $response = $this->getIndex();

        if(Request::input('default_paper_size')){
            DB::table('cms_settings')->where('name', 'default_paper_size')->update(['content' => $papersize]);
        }

        switch ($filetype){
            case "pdf":
                $view = view('crudbooster::export', $response)->render();
                $pdf = App::make('dompdf.wrapper');
                $pdf->loadHTML($view);
                $pdf->setPaper($papersize, $paperorientation);

                return $pdf->stream($filename.'.pdf');
                break;
            case 'xls':
                $view = view('crudbooster::export_custom', $response)->render();
                CB::exportXLS($filename,$view, true);
                break;
            case 'csv':
                Excel::create($filename, function ($excel) use ($response) {
                    $excel->setTitle($filename)->setCreator("crudbooster.com")->setCompany(CRUDBooster::getSetting('appname'));
                    $excel->sheet($filename, function ($sheet) use ($response) {
                        $sheet->setOrientation($paperorientation);
                        $sheet->loadview('crudbooster::export', $response);
                    });
                })->export('csv');
                break;
        }
    }

    public function postDataQuery()
    {
        $query = Request::get('query');
        $query = DB::select(DB::raw($query));

        return response()->json($query);
    }

    public function getDataTable()
    {
        $table = Request::get('table');
        $label = Request::get('label');
        $datatableWhere = urldecode(Request::get('datatable_where'));
        $foreign_key_name = Request::get('fk_name');
        $foreign_key_value = Request::get('fk_value');
        if ($table && $label && $foreign_key_name && $foreign_key_value) {
            $query = DB::table($table);
            if ($datatableWhere) {
                $query->whereRaw($datatableWhere);
            }
            $query->select('id as select_value', $label.' as select_label');
            $query->where($foreign_key_name, $foreign_key_value);
            $query->orderby($label, 'asc');

            return response()->json($query->get());
        } else {
            return response()->json([]);
        }
    }

    public function getModalData()
    {
        $table = Request::get('table');
        $where = Request::get('where');
        $where = urldecode($where);
        $columns = Request::get('columns');
        $columns = explode(",", $columns);

        $table = CRUDBooster::parseSqlTable($table)['table'];
        $tablePK = CB::pk($table);
        $result = DB::table($table);

        if (Request::get('q')) {
            $result->where(function ($where) use ($columns) {
                foreach ($columns as $c => $col) {
                    if ($c == 0) {
                        $where->where($col, 'like', '%'.Request::get('q').'%');
                    } else {
                        $where->orWhere($col, 'like', '%'.Request::get('q').'%');
                    }
                }
            });
        }

        if ($where) {
            $result->whereraw($where);
        }

        $result->orderby($tablePK, 'desc');

        $data['result'] = $result->paginate(6);
        $data['columns'] = $columns;

        return view('crudbooster::default.type_components.datamodal.browser', $data);
    }

    public function getUpdateSingle()
    {
        $table = Request::get('table');
        $column = Request::get('column');
        $value = Request::get('value');
        $id = Request::get('id');
        $tablePK = CB::pk($table);
        DB::table($table)->where($tablePK, $id)->update([$column => $value]);

        return redirect()->back()->with(['message_type' => 'success', 'message' => trans('crudbooster.alert_delete_data_success')]);
    }

    public function getFindData()
    {
        $q = Request::get('q');
        $id = Request::get('id');
        $limit = Request::get('limit') ?: 10;
        $format = Request::get('format');

        $table1 = (Request::get('table1')) ?: $this->table;
        $table1PK = CB::pk($table1);
        $column1 = (Request::get('column1')) ?: $this->title_field;

        @$table2 = Request::get('table2');
        @$column2 = Request::get('column2');

        @$table3 = Request::get('table3');
        @$column3 = Request::get('column3');

        $where = Request::get('where');

        $fk = Request::get('fk');
        $fk_value = Request::get('fk_value');

        if ($q || $id || $table1) {
            $rows = DB::table($table1);
            $rows->select($table1.'.*');
            $rows->take($limit);

            if (CRUDBooster::isColumnExists($table1, 'deleted_at')) {
                $rows->where($table1.'.deleted_at', null);
            }

            if ($fk && $fk_value) {
                $rows->where($table1.'.'.$fk, $fk_value);
            }

            if ($table1 && $column1) {

                $orderby_table = $table1;
                $orderby_column = $column1;
            }

            if ($table2 && $column2) {
                $table2PK = CB::pk($table2);
                $rows->join($table2, $table2.'.'.$table2PK, '=', $table1.'.'.$column1);
                $columns = CRUDBooster::getTableColumns($table2);
                foreach ($columns as $col) {
                    $rows->addselect($table2.".".$col." as ".$table2."_".$col);
                }
                $orderby_table = $table2;
                $orderby_column = $column2;
            }

            if ($table3 && $column3) {
                $table3PK = CB::pk($table3);
                $rows->join($table3, $table3.'.'.$table3PK, '=', $table2.'.'.$column2);
                $columns = CRUDBooster::getTableColumns($table3);
                foreach ($columns as $col) {
                    $rows->addselect($table3.".".$col." as ".$table3."_".$col);
                }
                $orderby_table = $table3;
                $orderby_column = $column3;
            }

            if ($id) {
                $rows->where($table1.".".$table1PK, $id);
            }

            if ($where) {
                $rows->whereraw($where);
            }

            if ($format) {
                $format = str_replace('&#039;', "'", $format);
                $rows->addselect(DB::raw("CONCAT($format) as text"));
                if ($q) {
                    $rows->whereraw("CONCAT($format) like '%".$q."%'");
                }
            } else {
                $rows->addselect($orderby_table.'.'.$orderby_column.' as text');
                if ($q) {
                    $rows->where($orderby_table.'.'.$orderby_column, 'like', '%'.$q.'%');
                }
                $rows->orderBy($orderby_table.'.'.$orderby_column, 'asc');
            }

            $result = [];
            $result['items'] = $rows->get();
        } else {
            $result = [];
            $result['items'] = [];
        }

        return response()->json($result);
    }

    public function validation($id = null)
    {

        $request_all = Request::all();
        $array_input = [];
        foreach ($this->data_inputan as $di) {
            $ai = [];
            $name = $di['name'];

            if (! isset($request_all[$name])) {
                continue;
            }

            if ($di['type'] != 'upload') {
                if (@$di['required']) {
                    $ai[] = 'required';
                }
            }

            if ($di['type'] == 'upload') {
                if ($id) {
                    $row = DB::table($this->table)->where($this->primary_key, $id)->first();
                    if ($row->{$di['name']} == '') {
                        $ai[] = 'required';
                    }
                }
            }

            if (@$di['min']) {
                $ai[] = 'min:'.$di['min'];
            }
            if (@$di['max']) {
                $ai[] = 'max:'.$di['max'];
            }
            if (@$di['image']) {
                $ai[] = 'image';
            }
            if (@$di['mimes']) {
                $ai[] = 'mimes:'.$di['mimes'];
            }
            $name = $di['name'];
            if (! $name) {
                continue;
            }

            if ($di['type'] == 'money') {
                $request_all[$name] = preg_replace('/[^\d-]+/', '', $request_all[$name]);
            }

            if ($di['type'] == 'child') {
                $slug_name = str_slug($di['label'], '');
                foreach ($di['columns'] as $child_col) {
                    if (isset($child_col['validation'])) {
                        //https://laracasts.com/discuss/channels/general-discussion/array-validation-is-not-working/
                        if (strpos($child_col['validation'], 'required') !== false) {
                            $array_input[$slug_name.'-'.$child_col['name']] = 'required';

                            str_replace('required', '', $child_col['validation']);
                        }

                        $array_input[$slug_name.'-'.$child_col['name'].'.*'] = $child_col['validation'];
                    }
                }
            }

            if (@$di['validation']) {

                $exp = explode('|', $di['validation']);
                if ($exp && count($exp)) {
                    foreach ($exp as &$validationItem) {
                        if (substr($validationItem, 0, 6) == 'unique') {
                            $parseUnique = explode(',', str_replace('unique:', '', $validationItem));
                            $uniqueTable = ($parseUnique[0]) ?: $this->table;
                            $uniqueColumn = ($parseUnique[1]) ?: $name;
                            $uniqueIgnoreId = ($parseUnique[2]) ?: (($id) ?: '');

                            //Make sure table name
                            $uniqueTable = CB::parseSqlTable($uniqueTable)['table'];

                            //Rebuild unique rule
                            $uniqueRebuild = [];
                            $uniqueRebuild[] = $uniqueTable;
                            $uniqueRebuild[] = $uniqueColumn;
                            if ($uniqueIgnoreId) {
                                $uniqueRebuild[] = $uniqueIgnoreId;
                            } else {
                                $uniqueRebuild[] = 'NULL';
                            }

                            //Check whether deleted_at exists or not
                            if (CB::isColumnExists($uniqueTable, 'deleted_at')) {
                                $uniqueRebuild[] = CB::findPrimaryKey($uniqueTable);
                                $uniqueRebuild[] = 'deleted_at';
                                $uniqueRebuild[] = 'NULL';
                            }
                            $uniqueRebuild = array_filter($uniqueRebuild);
                            $validationItem = 'unique:'.implode(',', $uniqueRebuild);
                        }
                    }
                } else {
                    $exp = [];
                }

                $validation = implode('|', $exp);

                $array_input[$name] = $validation;
            } else {
                $array_input[$name] = implode('|', $ai);
            }
        }

        $validator = Validator::make($request_all, $array_input);

        if ($validator->fails()) {
            $message = $validator->messages();
            $message_all = $message->all();

            if (Request::ajax()) {
                $res = response()->json([
                    'message' => trans('crudbooster.alert_validation_error', ['error' => implode(', ', $message_all)]),
                    'message_type' => 'warning',
                ])->send();
                exit;
            } else {
                $res = redirect()->back()->with("errors", $message)->with([
                    'message' => trans('crudbooster.alert_validation_error', ['error' => implode(', ', $message_all)]),
                    'message_type' => 'warning',
                ])->withInput();
                \Session::driver()->save();
                $res->send();
                exit;
            }
        }
    }

    public function input_assignment($id = null)
    {

        $hide_form = (Request::get('hide_form')) ? unserialize(Request::get('hide_form')) : [];

        foreach ($this->data_inputan as $ro) {
            $name = $ro['name'];

            if (! $name) {
                continue;
            }

            if ($ro['exception']) {
                continue;
            }

            if ($name == 'hide_form') {
                continue;
            }

            if ($ro['type'] == 'header') {
                continue;
            }

            if ($hide_form && count($hide_form)) {
                if (in_array($name, $hide_form)) {
                    continue;
                }
            }

            if ($ro['type'] == 'checkbox' && $ro['relationship_table']) {
                continue;
            }

            if ($ro['type'] == 'select2' && $ro['relationship_table']) {
                continue;
            }

            $inputdata = Request::get($name);

            if ($ro['type'] == 'money') {
                $inputdata = preg_replace('/[^\d-]+/', '', $inputdata);
            }

            if ($ro['type'] == 'child') {
                continue;
            }

            if ($name) {
                if ($inputdata != '') {
                    $this->arr[$name] = $inputdata;
                } else {
                    if (CB::isColumnNULL($this->table, $name) && $ro['type'] != 'upload') {
                        continue;
                    } else {
                        $this->arr[$name] = "";
                    }
                }
            }

            $password_candidate = explode(',', config('crudbooster.PASSWORD_FIELDS_CANDIDATE'));
            if (in_array($name, $password_candidate)) {
                if (! empty($this->arr[$name])) {
                    $this->arr[$name] = Hash::make($this->arr[$name]);
                } else {
                    unset($this->arr[$name]);
                }
            }

            if ($ro['type'] == 'checkbox') {

                if (is_array($inputdata)) {
                    if ($ro['datatable'] != '') {
                        $table_checkbox = explode(',', $ro['datatable'])[0];
                        $field_checkbox = explode(',', $ro['datatable'])[1];
                        $table_checkbox_pk = CB::pk($table_checkbox);
                        $data_checkbox = DB::table($table_checkbox)->whereIn($table_checkbox_pk, $inputdata)->pluck($field_checkbox)->toArray();
                        $this->arr[$name] = implode(";", $data_checkbox);
                    } else {
                        $this->arr[$name] = implode(";", $inputdata);
                    }
                }
            }

            //multitext colomn
            if ($ro['type'] == 'multitext') {
                $name = $ro['name'];
                $multitext = "";
                $maxI = ($this->arr[$name])?count($this->arr[$name]):0;
                for ($i = 0; $i <= $maxI - 1; $i++) {
                    $multitext .= $this->arr[$name][$i]."|";
                }
                $multitext = substr($multitext, 0, strlen($multitext) - 1);
                $this->arr[$name] = $multitext;
            }

            if ($ro['type'] == 'googlemaps') {
                if ($ro['latitude'] && $ro['longitude']) {
                    $latitude_name = $ro['latitude'];
                    $longitude_name = $ro['longitude'];
                    $this->arr[$latitude_name] = Request::get('input-latitude-'.$name);
                    $this->arr[$longitude_name] = Request::get('input-longitude-'.$name);
                }
            }

            if ($ro['type'] == 'select' || $ro['type'] == 'select2') {
                if ($ro['datatable']) {
                    if ($inputdata == '') {
                        $this->arr[$name] = 0;
                    }
                }
            }

            if (@$ro['type'] == 'upload') {

                $this->arr[$name] = CRUDBooster::uploadFile($name, $ro['encrypt'], $ro['resize_width'], $ro['resize_height'], CB::myId());

                if (! $this->arr[$name]) {
                    $this->arr[$name] = Request::get('_'.$name);
                }

                if($id && g('_'.$name)=='' && !request()->hasfile($name)) {
                    $oldImage = DB::table($this->table)->where($this->primary_key,$id)->first()->$name;
                    CB::removeFile($oldImage);
                    $this->arr[$name] = null;
                }
            }

            if (@$ro['type'] == 'filemanager') {
                $filename = str_replace('/'.config('lfm.prefix').'/'.config('lfm.files_folder_name').'/', '', $this->arr[$name]);
                $url = 'uploads/'.$filename;
                $this->arr[$name] = $url;
            }
        }
    }

    private function afterSaveJob($id)
    {
        foreach ($this->data_inputan as $ro) {
            $name = $ro['name'];
            if (! $name) {
                continue;
            }

            $inputdata = Request::get($name);

            //Insert Data Checkbox if Type Datatable
            if ($ro['type'] == 'checkbox') {
                if ($ro['relationship_table']) {
                    $datatable = explode(",", $ro['datatable'])[0];
                    $foreignKey2 = CRUDBooster::getForeignKey($datatable, $ro['relationship_table']);
                    $foreignKey = CRUDBooster::getForeignKey($this->table, $ro['relationship_table']);
                    DB::table($ro['relationship_table'])->where($foreignKey, $id)->delete();

                    if ($inputdata) {
                        $relationship_table_pk = CB::pk($ro['relationship_table']);
                        foreach ($inputdata as $input_id) {
                            DB::table($ro['relationship_table'])->insert([
//                                 $relationship_table_pk => CRUDBooster::newId($ro['relationship_table']),
                                $foreignKey => $id,
                                $foreignKey2 => $input_id,
                            ]);
                        }
                    }
                }
            }

            if ($ro['type'] == 'select2') {
                if ($ro['relationship_table']) {
                    $datatable = explode(",", $ro['datatable'])[0];
                    $foreignKey2 = CRUDBooster::getForeignKey($datatable, $ro['relationship_table']);
                    $foreignKey = CRUDBooster::getForeignKey($this->table, $ro['relationship_table']);
                    DB::table($ro['relationship_table'])->where($foreignKey, $id)->delete();

                    if ($inputdata) {
                        foreach ($inputdata as $input_id) {
                            $relationship_table_pk = CB::pk($row['relationship_table']);
                            DB::table($ro['relationship_table'])->insert([
//                                 $relationship_table_pk => CRUDBooster::newId($ro['relationship_table']),
                                $foreignKey => $id,
                                $foreignKey2 => $input_id,
                            ]);
                        }
                    }
                }
            }

            if ($ro['type'] == 'child') {
                $name = str_slug($ro['label'], '');
                $columns = $ro['columns'];
                $getColName = Request::get($name.'-'.$columns[0]['name']);
                $count_input_data = ($getColName)?(count($getColName) - 1):0;
                $child_array = [];

                for ($i = 0; $i <= $count_input_data; $i++) {
                    $fk = $ro['foreign_key'];
                    $column_data = [];
                    $column_data[$fk] = $id;
                    foreach ($columns as $col) {
                        $colname = $col['name'];
                        $column_data[$colname] = Request::get($name.'-'.$colname)[$i];
                    }
                    $child_array[] = $column_data;
                }

                $childtable = CRUDBooster::parseSqlTable($ro['table'])['table'];
                DB::table($childtable)->insert($child_array);
            }
        }
    }

    private $resolveDataBeforeSave = null;

    public function beforeSave($callback) {
        $this->resolveDataBeforeSave = $callback;
    }

    private $afterSave = null;

    public function afterSave($callback)
    {
        $this->afterSave = $callback;
    }

    public function saveHandler($id = null) {

        DB::beginTransaction();
        try{



            if (Schema::hasColumn($this->table, 'created_at')) {
                $this->arr['created_at'] = date('Y-m-d H:i:s');
            }

            if(isset($this->resolveDataBeforeSave)) {
                $this->arr = (array) call_user_func($this->resolveDataBeforeSave, (object) $this->arr);
            }

            if($id) {
                if (Schema::hasColumn($this->table, 'updated_at')) {
                    $this->arr['updated_at'] = date('Y-m-d H:i:s');
                }

                DB::table($this->table)->where($this->primary_key, $id)->update($this->arr);
            }else{
                $id = DB::table($this->table)->insertGetId($this->arr);
            }

            $this->afterSaveJob($id);

            if(isset($this->afterSave)) {
                call_user_func($this->afterSave, $id);
            }
            DB::commit();

            return $id;
        }catch (\Exception $e)
        {
            DB::rollback();
            echo $e->getMessage();
            echo '<hr/>';
            echo $e->getLine().' | '.$e->getFile();
            echo '<pre>';
            print_r($e->getTraceAsString());
            exit;
        }
    }

    public function getAdd()
    {
        $this->cbLoader();
        if (! CRUDBooster::isCreate() && $this->global_privilege == false || $this->button_add == false) {
            CRUDBooster::insertLog(trans('crudbooster.log_try_add', ['module' => CRUDBooster::getCurrentModule()->name]));
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans("crudbooster.denied_access"));
        }

        $page_title = $this->pageTitle?:trans("crudbooster.add_data_page_title", ['module' => CRUDBooster::getCurrentModule()->name]);
        $page_menu = Route::getCurrentRoute()->getActionName();
        $command = 'add';

        return view('crudbooster::default.form', compact('page_title', 'page_menu', 'command'));
    }

    public function postAddSave()
    {
        $this->cbLoader();
        if (! CRUDBooster::isCreate() && $this->global_privilege == false) {
            CRUDBooster::insertLog(trans('crudbooster.log_try_add_save', [
                'name' => Request::input($this->title_field),
                'module' => CRUDBooster::getCurrentModule()->name,
            ]));
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans("crudbooster.denied_access"));
        }

        $this->validation();

        $this->input_assignment();

        $this->hook_before_add($this->arr);

        $id = $this->saveHandler();

        $this->hook_after_add($id);

        $this->return_url = ($this->return_url) ? $this->return_url : Request::get('return_url');
        //insert log
        CRUDBooster::insertLog(trans("crudbooster.log_add", ['name' => $this->arr[$this->title_field], 'module' => CRUDBooster::getCurrentModule()->name]));

        if ($this->return_url) {
            if (Request::get('submit') == trans('crudbooster.button_save_more')) {
                CRUDBooster::redirect(Request::server('HTTP_REFERER'), trans("crudbooster.alert_add_data_success"), 'success');
            } else {
                CRUDBooster::redirect($this->return_url, trans("crudbooster.alert_add_data_success"), 'success');
            }
        } else {
            if (Request::get('submit') == trans('crudbooster.button_save_more')) {
                CRUDBooster::redirect(CRUDBooster::mainpath('add'), trans("crudbooster.alert_add_data_success"), 'success');
            } else {
                CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
            }
        }
    }

    public function getEdit($id)
    {
        $this->cbLoader();
        $row = DB::table($this->table)->where($this->primary_key, $id)->first();
        $this->current_row = $row;

        if (! CRUDBooster::isRead() && $this->global_privilege == false || $this->button_edit == false) {
            CRUDBooster::insertLog(trans("crudbooster.log_try_edit", [
                'name' => $row->{$this->title_field},
                'module' => CRUDBooster::getCurrentModule()->name,
            ]));
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
        }

        $page_menu = Route::getCurrentRoute()->getActionName();
        $page_title = $this->pageTitle?:trans("crudbooster.edit_data_page_title", ['module' => CRUDBooster::getCurrentModule()->name, 'name' => $row->{$this->title_field}]);
        $command = 'edit';
        Session::put('current_row_id', $id);

        return view('crudbooster::default.form', compact('id', 'row', 'page_menu', 'page_title', 'command'));
    }

    public function postEditSave($id)
    {

        $this->cbLoader();

        $row = DB::table($this->table)->where($this->primary_key, $id)->first();

        if (! CRUDBooster::isUpdate() && $this->global_privilege == false) {
            CRUDBooster::insertLog(trans("crudbooster.log_try_add", ['name' => $row->{$this->title_field}, 'module' => CRUDBooster::getCurrentModule()->name]));
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
        }

        $this->validation($id);

        $this->input_assignment($id);

        $this->hook_before_edit($this->arr,$id);

        $this->saveHandler($id);

        $this->hook_after_edit($id);

        $this->return_url = ($this->return_url) ? $this->return_url : Request::get('return_url');

        $old_values = json_decode(json_encode($row), true);
        CRUDBooster::insertLog(trans("crudbooster.log_update", [
            'name' => $this->arr[$this->title_field],
            'module' => CRUDBooster::getCurrentModule()->name,
        ]), LogsController::displayDiff($old_values, $this->arr));

        if ($this->return_url) {
            CRUDBooster::redirect($this->return_url, trans("crudbooster.alert_update_data_success"), 'success');
        } else {
            if (Request::get('submit') == trans('crudbooster.button_save_more')) {
                CRUDBooster::redirect(CRUDBooster::mainpath('add'), trans("crudbooster.alert_update_data_success"), 'success');
            } else {
                CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_update_data_success"), 'success');
            }
        }
    }

    public function getDelete($id)
    {
        $this->cbLoader();
        $row = DB::table($this->table)->where($this->primary_key, $id)->first();

        if (! CRUDBooster::isDelete() && $this->global_privilege == false || $this->button_delete == false) {
            CRUDBooster::insertLog(trans("crudbooster.log_try_delete", [
                'name' => $row->{$this->title_field},
                'module' => CRUDBooster::getCurrentModule()->name,
            ]));
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
        }

        //insert log
        CRUDBooster::insertLog(trans("crudbooster.log_delete", ['name' => $row->{$this->title_field}, 'module' => CRUDBooster::getCurrentModule()->name]));

        $this->hook_before_delete($id);

        $updated['deleted_at'] = date('Y-m-d H:i:s');
        DB::table($this->table)->where($this->primary_key, $id)->update($updated);
        // DB::table($this->table)->where($this->primary_key, $id)->delete();

        $this->hook_after_delete($id);

        $url = g('return_url') ?: CRUDBooster::referer();

        CRUDBooster::redirect($url, trans("crudbooster.alert_delete_data_success"), 'success');
    }

    public function getDetail($id)
    {
        $this->cbLoader();
        $row = DB::table($this->table)->where($this->primary_key, $id)->first();
        $this->current_row = $row;

        if (! CRUDBooster::isRead() && $this->global_privilege == false || $this->button_detail == false) {
            CRUDBooster::insertLog(trans("crudbooster.log_try_view", [
                'name' => $row->{$this->title_field},
                'module' => CRUDBooster::getCurrentModule()->name,
            ]));
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
        }

        $module = CRUDBooster::getCurrentModule();

        $page_menu = Route::getCurrentRoute()->getActionName();
        $page_title = $this->pageTitle?:trans("crudbooster.detail_data_page_title", ['module' => $module->name, 'name' => $row->{$this->title_field}]);
        $command = 'detail';

        Session::put('current_row_id', $id);

        $data = [];
        $data['row'] = $row;
        $data['page_menu'] = $page_menu;
        $data['page_title'] = $page_title;
        $data['command'] = $command;
        $data['id'] = $id;

        $data['viewAfterDetail'] = $this->viewAfterDetail;
        $this->viewAfterDetailData['row'] = $row;
        $data['viewAfterDetailData'] = $this->viewAfterDetailData;

        return view('crudbooster::default.form',$data);
    }

    public function getImportData()
    {
        $this->cbLoader();
        $data['page_menu'] = Route::getCurrentRoute()->getActionName();
        $data['page_title'] = 'Import Data '.$module->name;

        if (Request::get('file') && ! Request::get('import')) {
            $file = base64_decode(Request::get('file'));
            $file = storage_path('app/'.$file);
            $rows = Excel::load($file, function ($reader) {
            })->get();
            
            $countRows = ($rows)?count($rows):0;
            
            Session::put('total_data_import', $countRows);

            $data_import_column = [];
            foreach ($rows as $value) {
                $a = [];
                foreach ($value as $k => $v) {
                    $a[] = $k;
                }
                if ($a && count($a)) {
                    $data_import_column = $a;
                }
                break;
            }

            $table_columns = DB::getSchemaBuilder()->getColumnListing($this->table);

            $data['table_columns'] = $table_columns;
            $data['data_import_column'] = $data_import_column;
        }

        return view('crudbooster::import', $data);
    }

    public function postDoneImport()
    {
        $this->cbLoader();
        $data['page_menu'] = Route::getCurrentRoute()->getActionName();
        $data['page_title'] = trans('crudbooster.import_page_title', ['module' => $module->name]);
        Session::put('select_column', Request::get('select_column'));

        return view('crudbooster::import', $data);
    }

    public function postDoImportChunk()
    {
        $this->cbLoader();
        $file_md5 = md5(Request::get('file'));

        if (Request::get('file') && Request::get('resume') == 1) {
            $total = Session::get('total_data_import');
            $prog = intval(Cache::get('success_'.$file_md5)) / $total * 100;
            $prog = round($prog, 2);
            if ($prog >= 100) {
                Cache::forget('success_'.$file_md5);
            }

            return response()->json(['progress' => $prog, 'last_error' => Cache::get('error_'.$file_md5)]);
        }

        $select_column = Session::get('select_column');
        $select_column = array_filter($select_column);
        $table_columns = DB::getSchemaBuilder()->getColumnListing($this->table);

        $file = base64_decode(Request::get('file'));
        $file = storage_path('app/'.$file);

        $rows = Excel::load($file, function ($reader) {
        })->get();

        $has_created_at = false;
        if (CRUDBooster::isColumnExists($this->table, 'created_at')) {
            $has_created_at = true;
        }

        $data_import_column = [];
        foreach ($rows as $value) {
            $a = [];
            foreach ($select_column as $sk => $s) {
                $colname = $table_columns[$sk];

                if (CRUDBooster::isForeignKey($colname)) {

                    //Skip if value is empty
                    if ($value->$s == '') {
                        continue;
                    }

                    if (intval($value->$s)) {
                        $a[$colname] = $value->$s;
                    } else {
                        $relation_table = CRUDBooster::getTableForeignKey($colname);
                        $relation_moduls = DB::table('cms_moduls')->where('table_name', $relation_table)->first();

                        $relation_class = __NAMESPACE__.'\\'.$relation_moduls->controller;
                        if (! class_exists($relation_class)) {
                            $relation_class = '\App\Http\Controllers\\'.$relation_moduls->controller;
                        }
                        $relation_class = new $relation_class;
                        $relation_class->cbLoader();

                        $title_field = $relation_class->title_field;

                        $relation_insert_data = [];
                        $relation_insert_data[$title_field] = $value->$s;

                        if (CRUDBooster::isColumnExists($relation_table, 'created_at')) {
                            $relation_insert_data['created_at'] = date('Y-m-d H:i:s');
                        }

                        try {
                            $relation_exists = DB::table($relation_table)->where($title_field, $value->$s)->first();
                            if ($relation_exists) {
                                $relation_primary_key = $relation_class->primary_key;
                                $relation_id = $relation_exists->$relation_primary_key;
                            } else {
                                $relation_id = DB::table($relation_table)->insertGetId($relation_insert_data);
                            }

                            $a[$colname] = $relation_id;
                        } catch (\Exception $e) {
                            exit($e);
                        }
                    } //END IS INT

                } else {
                    $a[$colname] = $value->$s;
                }
            }

            $has_title_field = true;
            foreach ($a as $k => $v) {
                if ($k == $this->title_field && $v == '') {
                    $has_title_field = false;
                    break;
                }
            }

            if ($has_title_field == false) {
                continue;
            }

            try {

                if ($has_created_at) {
                    $a['created_at'] = date('Y-m-d H:i:s');
                }   

                DB::table($this->table)->insert($a);
                Cache::increment('success_'.$file_md5);
            } catch (\Exception $e) {
                $e = (string) $e;
                Cache::put('error_'.$file_md5, $e, 500);
            }
        }

        return response()->json(['status' => true]);
    }

    public function postDoUploadImportData()
    {
        $this->cbLoader();
        if (Request::hasFile('userfile')) {
            $file = Request::file('userfile');
            $ext = $file->getClientOriginalExtension();

            $validator = Validator::make([
                'extension' => $ext,
            ], [
                'extension' => 'in:xls,xlsx,csv',
            ]);

            if ($validator->fails()) {
                $message = $validator->errors()->all();

                return redirect()->back()->with(['message' => implode('<br/>', $message), 'message_type' => 'warning']);
            }

            //Create Directory Monthly
            $filePath = 'uploads/'.CB::myId().'/'.date('Y-m');
            Storage::makeDirectory($filePath);

            //Move file to storage
            $filename = md5(str_random(5)).'.'.$ext;
            $url_filename = '';
            if (Storage::putFileAs($filePath, $file, $filename)) {
                $url_filename = $filePath.'/'.$filename;
            }
            $url = CRUDBooster::mainpath('import-data').'?file='.base64_encode($url_filename);

            return redirect($url);
        } else {
            return redirect()->back();
        }
    }

    public function postActionSelected()
    {
        $this->cbLoader();
        $id_selected = Request::input('checkbox');
        $button_name = Request::input('button_name');

        if (! $id_selected) {
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_select_a_data"), 'warning');
        }

        if ($button_name == 'delete') {

            if (! CRUDBooster::isDelete()) {
                CRUDBooster::insertLog(trans("crudbooster.log_try_delete_selected", ['module' => CRUDBooster::getCurrentModule()->name]));
                CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
            }

            $this->hook_before_delete($id_selected);
            $tablePK = CB::pk($this->table);
            //bulk man
            foreach($id_selected as $is){
                if($this->table=='production_formula'){
                    $pf = DB::table('production_formula')->where('id',$is)->first();
                    DB::table('products')->where('id',$pf->id_products)->delete();
                }if($this->table=='purchases'){
                    $p = DB::table('purchases')->where('id',$is)->first();
                    DB::table('purchases_detail')->where('id_purchases',$p->id)->delete();
                }if($this->table=='sellings'){
                    $p = DB::table('sellings')->where('id',$is)->first();
                    DB::table('sellings_detail')->where('id_sellings',$p->id)->delete();
                }if($this->table=='production'){
                    $p = DB::table('production')->where('id',$is)->first();
                    updatePriceGlobal($is);
                    DB::table('production_detail')->where('id_production',$p->id)->delete();
                    DB::table('production_products_non_finished')->where('id_production',$p->id)->delete();
                }if($this->table=='disbursement_deposits'){
                    $p = DB::table('disbursement_deposits')->where('id',$is)->first();
                    DB::table('disbursement_deposits_detail')->where('id_disbursement_deposits',$p->id)->delete();
                }if($this->table=='costs'){
                    $p = DB::table('costs')->where('id',$is)->first();
                    DB::table('costs_detail')->where('id_costs',$p->id)->delete();
                }

                // master golongan
                if($this->table=='type'){
                    $is_no_delete = 'yes';
                    $cek = DB::table('products')->where('type_id',$is)->first();
                    $joined = 'Produk';

                    if($cek){
                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->type_name.', </b>';
                        $is_true .= 'gagal';
                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master grup
                if($this->table=='groups'){
                    $is_no_delete = 'yes';
                    $cek = DB::table('products')->where('groups_id',$is)->first();
                    $joined = 'Produk';

                    if($cek){
                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->groups_name.', </b>';
                        $is_true .= 'gagal';
                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master jurnal
                if($this->table=='journals'){
                    $is_no_delete = 'yes';
                    $cek = DB::table('costs_detail')->where('id_journals',$is)->first();
                    $joined = 'Biaya';

                    if($cek){
                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name_journals.', </b>';
                        $is_true .= 'gagal';
                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master bank
                if($this->table=='bank'){
                    $is_no_delete = 'yes';
                    $joined = 'Pembayaran nota atau Pembayaran hutang';

                    $cek_np = DB::table('note_payment_account')->where('id_bank',$is)->first();
                    $cek_dp = DB::table('debt_payment_account')->where('id_bank',$is)->first();
                    if($cek_np!='' OR $cek_dp!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name_bank.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master tipe gudang
                if($this->table=='gudang_type'){
                    $is_no_delete = 'yes';
                    $joined = 'Gudang';

                    $cek = DB::table('gudang')->where('id_gudang_type',$is)->first();
                    if($cek!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->type.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }   

                // master customer golongan
                if($this->table=='master_customers_group'){
                    $is_no_delete = 'yes';
                    $joined = 'Customer';

                    $cek = DB::table('customers')->where('id_master_customers_group',$is)->first();
                    if($cek!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name_group.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }   

                // master customer area
                if($this->table=='master_customers_area'){
                    $is_no_delete = 'yes';
                    $joined = 'Customer';

                    $cek = DB::table('customers')->where('id_master_customers_area',$is)->first();
                    if($cek!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name_area.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master customer kota
                if($this->table=='master_customers_area'){
                    $is_no_delete = 'yes';
                    $joined = 'Customer';

                    $cek = DB::table('customers')->where('id_master_customers_city',$is)->first();
                    if($cek!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name_city.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master supplier grup
                if($this->table=='master_suppliers_group'){
                    $is_no_delete = 'yes';
                    $joined = 'Supplier';

                    $cek = DB::table('suppliers')->where('master_suppliers_group',$is)->first();
                    if($cek!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name_group.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master supplier area
                if($this->table=='master_suppliers_area'){
                    $is_no_delete = 'yes';
                    $joined = 'Supplier';

                    $cek = DB::table('suppliers')->where('master_suppliers_area',$is)->first();
                    if($cek!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name_area.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master supplier kota
                if($this->table=='id_master_suppliers_city'){
                    $is_no_delete = 'yes';
                    $joined = 'Supplier';

                    $cek = DB::table('suppliers')->where('id_master_suppliers_city',$is)->first();
                    if($cek!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name_city.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master gudang
                if($this->table=='gudang'){
                    $is_no_delete = 'yes';
                    $joined = 'Pembelian atau Penjualan';

                    $pur = DB::table('purchases')->where('id_gudang',$is)->first();
                    $sel = DB::table('sellings')->where('id_gudang',$is)->first();

                    if($pur!='' OR $sel!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master akun
                if($this->table=='accounts'){
                    $is_no_delete = 'yes';
                    $joined = 'Pembayaran Nota, Pembayaran Hutang, atau Pencairan Giro';

                    $cek_np = DB::table('note_payment_account')->where('id_accounts',$is)->first();
                    $cek_dp = DB::table('debt_payment_account')->where('id_accounts',$is)->first();
                    $pencairan = DB::table('disbursement_deposits')->where('id_accounts',$is)->first();

                    if($cek_np!='' OR $cek_dp!='' OR $pencairan!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->account_name.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master products
                if($this->table=='products'){
                    $is_no_delete = 'yes';
                    $joined = 'Pembelian, Penjualan, Produksi, Rumus Produksi, atau Bahan Rumus Produksi';

                    $pd = DB::table('purchases_detail')->where('id_products',$is)->first();
                    $sd = DB::table('sellings_detail')->where('id_products',$is)->first();
                    $prd = DB::table('production_detail')->where('id_products',$is)->first();
                    $pf = DB::table('production_formula')->where('id_products',$is)->first();
                    $pfd = DB::table('production_formula_detail')->where('id_products',$is)->first();

                    if($pd!='' OR $sd!='' OR $prd!='' OR $pf!='' OR $pfd!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master supplier
                if($this->table=='suppliers'){
                    $is_no_delete = 'yes';
                    $joined = 'Pembelian, Pembayaran Hutang';

                    $p = DB::table('purchases')->where('id_suppliers',$is)->first();
                    $dp = DB::table('debt_payment')->where('id_suppliers',$is)->first();

                    if($p!='' OR $dp!=''){

                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name.', </b>';
                        $is_true .= 'gagal';

                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }

                // master supplier
                if($this->table=='customers'){
                    $is_no_delete = 'yes';
                    $joined = 'Penjualan, Pembayaran Nota';

                    $s  = DB::table('sellings')->where('id_customers',$is)->first();
                    $np = DB::table('note_payment')->where('id',$is)->first();

                    if($s!='' OR $np!=''){
                        $data_master = DB::table($this->table)->where($tablePK, $is)->first();
                        $konten .= '<b>'.$data_master->name.', </b>';
                        $is_true .= 'gagal';
                    }else{
                        DB::table($this->table)->where($tablePK, $is)->delete();
                    }
                }


            }

            // fungsi inti delete
            if($is_no_delete!='yes'){
                DB::table($this->table)->whereIn($tablePK, $id_selected)->delete();
            }else{
                if(strpos($is_true, 'gagal') !== false){
                    warningBulk($konten,$joined);
                }else{
                    success();
                }
            }
            // close fungsi inti delete

            CRUDBooster::insertLog(trans("crudbooster.log_delete", ['name' => implode(',', $id_selected), 'module' => CRUDBooster::getCurrentModule()->name]));

            $this->hook_after_delete($id_selected);

            $message = trans("crudbooster.alert_delete_selected_success");

            return redirect()->back()->with(['message_type' => 'success', 'message' => $message]);
        }

        $action = str_replace(['-', '_'], ' ', $button_name);
        $action = ucwords($action);
        $type = 'success';
        $message = trans("crudbooster.alert_action", ['action' => $action]);

        if ($this->actionButtonSelected($id_selected, $button_name) === false) {
            $message = ! empty($this->alert['message']) ? $this->alert['message'] : 'Error';
            $type = ! empty($this->alert['type']) ? $this->alert['type'] : 'danger';
        }

        return redirect()->back()->with(['message_type' => $type, 'message' => $message]);
    }

    public function getDeleteImage()
    {
        $this->cbLoader();
        $id = Request::get('id');
        $column = Request::get('column');

        $row = DB::table($this->table)->where($this->primary_key, $id)->first();

        if (! CRUDBooster::isDelete() && $this->global_privilege == false) {
            CRUDBooster::insertLog(trans("crudbooster.log_try_delete_image", [
                'name' => $row->{$this->title_field},
                'module' => CRUDBooster::getCurrentModule()->name,
            ]));
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));
        }

        $row = DB::table($this->table)->where($this->primary_key, $id)->first();

        CB::removeFile($row->{$column});

        DB::table($this->table)->where($this->primary_key, $id)->update([$column => null]);

        CRUDBooster::insertLog(trans("crudbooster.log_delete_image", [
            'name' => $row->{$this->title_field},
            'module' => CRUDBooster::getCurrentModule()->name,
        ]));

        CRUDBooster::redirect(Request::server('HTTP_REFERER'), trans('crudbooster.alert_delete_data_success'), 'success');
    }

    public function postUploadSummernote()
    {
        $this->cbLoader();
        $name = 'userfile';
        if ($file = CRUDBooster::uploadFile($name, true)) {
            echo asset($file);
        }
    }

    public function postUploadFile()
    {
        $this->cbLoader();
        $name = 'userfile';
        if ($file = CRUDBooster::uploadFile($name, true)) {
            echo asset($file);
        }
    }

    public function postGeneratorHandle()
    {
        $this->cbLoader();
    }

    public function actionButtonSelected($id_selected, $button_name)
    {
    }

    /**
     * @param $query \Illuminate\Database\Query\Builder
     */
    public function hook_query_index(&$query)
    {
    }

    public function hook_row_index($index, &$value)
    {
    }

    public function hook_before_add(&$arr)
    {
    }

    public function hook_after_add($id)
    {
    }

    public function hook_before_edit(&$arr, $id)
    {
    }

    public function hook_after_edit($id)
    {
    }

    public function hook_before_delete($id)
    {
    }

    public function hook_after_delete($id)
    {
    }


    public function viewAfterDetail($view, $data = [])
    {
        $this->viewAfterDetail = $view;
        $this->viewAfterDetailData = $data;
    }
}
