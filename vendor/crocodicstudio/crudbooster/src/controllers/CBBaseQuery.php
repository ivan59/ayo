<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/17/2018
 * Time: 4:37 PM
 */

namespace crocodicstudio\crudbooster\controllers;

use Closure;
use crocodicstudio\crudbooster\helpers\CB;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\Paginator;
use Schema;
use DB;

class CBBaseQuery
{
    private $table=null;
    private $primaryKey=null;
    private $hookQuery=null;
    private $keyword = null;
    private $columns = [];
    private $scaffolding = [];
    private $resultQuery = null;

    public function __construct($scaffolding, $table)
    {
        $this->scaffolding = $scaffolding;
        $this->setTable($table);
        $this->initQuery();
    }

    public function setTable($table) {
        $this->table = $table;
        $this->primaryKey = CB::findPrimaryKey($this->table);
        return $this;
    }

    private function saveColumnsTemporary($table)
    {
        $this->columns[$table] = CB::getTableColumns($table);
    }

    public function hookQuery(Closure $closure) {
        $this->hookQuery = $closure;
    }

    public function having($key, $value) {
        if(isset($this->resultQuery)) {
            $this->resultQuery->having($key, $value);
        }
    }

    public function between($key, $start, $end= null) {
        if(isset($this->resultQuery)) {
            $this->resultQuery->where($key, ">=", $start);
            if($end) {
                $this->resultQuery->where($key,"<=", $end);
            }
        }
    }

    public function like($key, $value) {
        if(isset($this->resultQuery)) {
            if(!str_contains($key,'.')) {
                $key = $this->table.'.'.$key;
            }
            $this->resultQuery->where($key,'like','%'.$value.'%');
        }
    }

    public function find($keyword) {
        $this->resultQuery->where(function ($w) use ($keyword) {
            foreach($this->scaffolding as $i=>$column) {
                if(!str_contains($column['name'],'.')) {
                    $field = $this->table.'.'.$column['name'];
                }else{
                    $field = $column['name'];
                }
                if($i==0) {
                    $w->where($field, 'like', '%'.$keyword.'%');
                }else{
                    $w->orwhere($field, 'like', '%'.$keyword.'%');
                }

            }
        });
        return $this;
    }

    public function orderBy($field, $name) {
        $field = filter_var($field, FILTER_SANITIZE_STRING);
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        if($name == 'asc' || $name == 'desc') {
            $this->resultQuery->orderBy($field, $name);
        }
    }

    private function softDelete() {
        if(Schema::hasColumn($this->table, 'deleted_at')) {
            $this->resultQuery->whereNull($this->table.'.deleted_at');
        }
    }


    /**
     * @return Builder
     */
    public function getInstanceQuery() {
        return $this->resultQuery;
    }

    public function setInstanceQuery(Builder $query)
    {
        $this->resultQuery = $query;
    }

    private function initQuery()
    {
        if(!isset($this->table) || !isset($this->primaryKey)) {
            throw new \Exception("table and primary key are empty!");
        }

        $this->resultQuery = DB::table($this->table)->select(DB::raw($this->table.".".$this->primaryKey));

        //Add Select Base Table Fields
        $this->selectAllFieldFromTable($this->table, true);

        //Joining all columns that want to join
        $this->joiningColumns();

        //check for soft deletes
        $this->softDelete();

        //handle hook query
        if(isset($this->hookQuery)) {
            call_user_func($this->hookQuery, $this->resultQuery);
        }
    }

    public function get()
    {
        if(isset($this->resultQuery)) {
            return $this->resultQuery->get();
        }else{
            return null;
        }
    }

    public function paginate($limit)
    {
        if(isset($this->resultQuery)) {
            return $this->resultQuery->paginate($limit);
        }else{
            return null;
        }
    }

    private function joiningColumns()
    {
        foreach($this->scaffolding as $column) {
            if(isset($column['join'])) {
                $joinArray = explode(',', $column['join']);
                if(isset($joinArray[0]) && isset($joinArray[1])) {
                    $joinPK = CB::findPrimaryKey($joinArray[0]);
                    $this->resultQuery->leftjoin($joinArray[0],$joinArray[0].'.'.$joinPK,'=',$joinArray[1]);
                    $this->selectAllFieldFromTable($joinArray[0]);
                }
            }
        }
    }

    private function selectAllFieldFromTable($table, $noAliasOnly = false)
    {
        foreach(CB::getTableColumns($table) as $column) {
            if($noAliasOnly) {
                $this->resultQuery->addselect($table.'.'.$column);
            }
            $this->resultQuery->addselect($table.'.'.$column.' as '.$table.'_'.$column);
        }
    }
}