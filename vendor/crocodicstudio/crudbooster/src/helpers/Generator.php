<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/10/2018
 * Time: 9:40 AM
 */

namespace crocodicstudio\crudbooster\helpers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;

class Generator
{
    private static $data;

    public static function table($table)
    {
        self::$data['table'] = $table;
        return new self();
    }

    public function backURL($url) {
        self::$data['back_url'] = $url;
        return new self();
    }

    public function tableQuery($query)
    {
        self::$data['tableQuery'] = call_user_func($query,DB::table(self::$data['table']));
        return new self();
    }

    public function primaryKey($field)
    {
        self::$data['primaryKey'] = $field;
        return new self();
    }

    private function primaryField()
    {
        return self::$data['table'].'.'.self::$data['primaryKey'];
    }

    public function title($title)
    {
        self::$data['title'] = $title;
        self::$data['uniqueID'] = studly_case($title);
        return new self();
    }

    public function addCol($array)
    {
        self::$data['columns'][] = $array;
        return new self();
    }

    public function addForm($array)
    {
        self::$data['forms'][] = $array;
        return new self();
    }

    public function render($standaloneMode = false)
    {
        $this->actionController();

        $data = self::$data;
        $data['result'] = $this->makeTableQuery();

        if($standaloneMode) {
            $data['url'] = request()->fullUrl();
        }else{
            $data['url'] = CB::mainpath('generator-handle');
        }

        return view("crudbooster::generator.table",$data)->render();
    }

    public function renderView() {
        $data['content'] = $this->render( true );
        return view('crudbooster::generator.render_view',$data);
    }

    public function beforeSave($callback)
    {
        self::$data['beforeSave'] = $callback;
        return new self();
    }

    public function afterSave($callback)
    {
        self::$data['afterSave'] = $callback;
        return new self();
    }


    protected function actionController()
    {
        if(request('_generator')) {
            if(request('_generator') == self::$data['uniqueID']) {
                if(request('_action')) {
                    $action = request('_action');
                    switch ($action) {
                        case "add":
                            $data = [];
                            $data['forms'] = self::$data['forms'];
                            echo view("crudbooster::generator.form",$data)->render();
                            break;
                        case "addSave":
                            $saver = new CBController();
                            $saver->table = self::$data['table'];
                            $saver->primary_key = self::$data['primaryKey'];
                            $saver->form = self::$data['forms'];
                            $saver->cbLoader();
                            if(isset(self::$data['beforeSave'])) {
                                $saver->beforeSave(self::$data['beforeSave']);
                            }
                            if(isset(self::$data['afterSave'])) {
                                $saver->afterSave(self::$data['afterSave']);
                            }
                            $saver->validation();
                            $saver->input_assignment();
                            $saver->saveHandler();

                            CB::goBack("The data has been added!");
                            break;
                        case "edit":
                            $data = [];
                            $data['forms'] = self::$data['forms'];
                            $data['row'] = DB::table(self::$data['table'])->where(self::primaryField(),request('id'))->first();
                            echo view("crudbooster::generator.form",$data)->render();
                            break;
                        case "editSave":

                            $saver = new CBController();
                            $saver->table = self::$data['table'];
                            $saver->primary_key = self::$data['primaryKey'];
                            $saver->form = self::$data['forms'];
                            $saver->cbLoader();
                            if(isset(self::$data['beforeSave'])) {
                                $saver->beforeSave(self::$data['beforeSave']);
                            }
                            if(isset(self::$data['afterSave'])) {
                                $saver->afterSave(self::$data['afterSave']);
                            }
                            $saver->validation();
                            $saver->input_assignment();
                            $saver->saveHandler(request('id'));

                            CB::goBack("The data has been updated!");
                            break;
                        case 'delete':
                            DB::table(self::$data['table'])->where(self::primaryField(),request('id'))->delete();
                            CB::goBack("The data has been deleted!");
                            break;
                    }
                }
                exit;
            }
        }
    }


    protected function makeTableQuery() {
        return self::$data['tableQuery']->get();
    }
}