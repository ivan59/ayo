<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/9/2018
 * Time: 4:10 PM
 */

namespace crocodicstudio\crudbooster\helpers\Bootstrap3;


/**
 * Class SingleButtonDropDown
 * @package crocodicstudio\crudbooster\helpers
 */
class SingleButtonDropDown
{

    private static $color   = 'default';
    private static $value   = null;
    private static $options = [];

    /**
     * @return SingleButtonDropDown
     */
    public static function colorWhite() {
        static::$options = [];
        static::$color = 'default';
        return new static;
    }

    /**
     * @return SingleButtonDropDown
     */
    public static function colorBlue()
    {
        static::$options = [];
        static::$color = 'primary';
        return new static;
    }

    /**
     * @return SingleButtonDropDown
     */
    public static function colorGreen()
    {
        static::$options = [];
        static::$color = 'success';
        return new static;
    }

    /**
     * @return SingleButtonDropDown
     */
    public static function colorYellow()
    {
        static::$options = [];
        static::$color = 'warning';
        return new static;
    }

    /**
     * @return SingleButtonDropDown
     */
    public static function colorRed()
    {
        static::$options = [];
        static::$color = 'danger';
        return new static;
    }

    /**
     * @param $value
     * @return SingleButtonDropDown
     */
    public static function value($value)
    {
        static::$value = $value;
        return new static;
    }

    /**
     * @param $url
     * @param $label
     * @return SingleButtonDropDown
     */
    public static function option($url, $label)
    {
        static::$options[] = ['url'=>$url,'label'=>$label];
        return new static;
    }

    /**
     * @return string
     */
    public static function render()
    {
        $html = "
        <div class=\"btn-group\">
          <button type=\"button\" class=\"btn btn-".static::$color." btn-xs dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
            ".static::$value." <span class=\"caret\"></span>
          </button>
          <ul class=\"dropdown-menu\">";
        foreach(static::$options as $opt) {
            $html .= "<li><a href=\"".$opt['url']."\">".$opt['label']."</a></li>";
        }

        $html .= "  </ul>
        </div>";
        return $html;
    }

}