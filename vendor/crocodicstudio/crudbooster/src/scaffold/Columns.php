<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/19/2018
 * Time: 6:13 PM
 */

namespace crocodicstudio\crudbooster\scaffold;


class Columns
{
    private static $cols = [];

    private static function putSingleton() {
        app('crudbooster_columns')->addColumn(static::$cols['label'], static::$cols);
    }

    private static function makeAlias($name) {
        return str_replace('.','_',$name);
    }

    /**
     * @param $label
     * @param $name
     * @return Columns
     */
    public static function add($label, $name) {
        static::$cols = [];

        static::$cols['alias'] = static::makeAlias($name);
        static::$cols['name'] = $name;
        static::$cols['label'] = $label;

        static::putSingleton();
        return new static();
    }

    /**
     * @param string $type
     * @return Columns
     */

    public function filterBy($type) {
        static::$cols['filter_by'] = $type;
        static::putSingleton();
        return new static();
    }

    public function align($align = 'left') {
        static::$cols['align'] = $align;
        static::putSingleton();
        return new static();
    }

    public function prefix($prefix) {
        static::$cols['prefix'] = $prefix;
        static::putSingleton();
        return new static();
    }

    public function suffix($suffix) {
        static::$cols['suffix'] = $suffix;
        static::putSingleton();
        return new static();
    }

    /**
     * @return Columns
     */
    public function thumbnail() {
        static::$cols['image'] = true;
        static::putSingleton();
        return $this;
    }

    /**
     * @return Columns
     */
    public function imageable() {
        return $this->thumbnail();
    }

    /**
     * @return Columns
     */
    public function downloadable() {
        static::$cols['download'] = true;
        static::putSingleton();
        return $this;
    }

    /**
     * @return Columns
     */
    public function nl2br() {
        static::$cols['nl2br'] = true;
        static::putSingleton();
        return $this;
    }

    /**
     * @param $limit
     * @return Columns
     */
    public function strLimit($limit) {
        static::$cols['str_limit'] = $limit;
        static::putSingleton();
        return $this;
    }

    /**
     * @param $format
     * @return Columns
     */
    public function dateFormat($format) {
        static::$cols['date_format'] = $format;
        static::putSingleton();
        return $this;
    }

    /**
     * @param callable $callback
     * @return Columns
     */
    public function transform(Callable $callback) {
        static::$cols['callback'] = $callback;
        static::putSingleton();
        return $this;
    }


    public function numberFormat($dec = 0, $dec_point = '.', $thousands_sep = ",") {
        $name = static::$cols['name'];
        (new static())->transform(function ($row) use ($name, $dec, $dec_point, $thousands_sep) {
           return number_format($row->$name, $dec, $dec_point, $thousands_sep);
        });
        static::putSingleton();
        return $this;
    }

    /**
     * @param $relationTable
     * @param $sourceRelationField
     * @return Columns
     */
    public function fetchFrom($relationTable, $sourceRelationField) {
        static::$cols['join'] = $relationTable.",".$sourceRelationField;
        if(isset(static::$cols['name'])) {
            if(str_contains(static::$cols['name'],'.')) {
                static::$cols['name'] = $relationTable.'.'.(end(explode(',',static::$cols['name'])));
                static::$cols['alias'] = static::makeAlias(static::$cols['name']);
            }else{
                static::$cols['name'] = $relationTable.'.'.static::$cols['name'];
                static::$cols['alias'] = static::makeAlias(static::$cols['name']);
            }
        }
        static::putSingleton();
        return $this;
    }

    /**
     * @return Columns
     */
    public function hide()
    {
        static::$cols['visible'] = false;
        static::putSingleton();
        return $this;
    }
}