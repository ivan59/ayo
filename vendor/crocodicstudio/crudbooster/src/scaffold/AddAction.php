<?php

namespace crocodicstudio\crudbooster\scaffold;

use crocodicstudio\crudbooster\helpers\CRUDBooster;

class AddAction
{
    private static $cols = [];

    private static function makeAlias($name) {
        return str_replace('.','_',$name);
    }

    private static function putSingleton() {
        $label = static::$cols['label']?:static::$cols['title'];
        app('crudbooster_columns')->addAddAction($label, static::$cols);
    }

    /**
     * @param $label
     * @param $name
     * @return AddAction
     */
    public static function add($label) {
        static::$cols = [];

        static::$cols['alias'] = static::makeAlias($label);
        static::$cols['label'] = $label;

        static::putSingleton();
        return new static();
    }

    public function asButtonText()
    {
        static::$cols['title'] = static::$cols['label'];

        static::putSingleton();
        return new static();
    }

    public function asButtonIcon()
    {
        static::$cols['title'] = static::$cols['label'];
        static::$cols['label'] = "";

        static::putSingleton();
        return new static();
    }

    public function icon($icon)
    {
        static::$cols['icon'] = $icon;
        static::putSingleton();
        return new static();
    }

    public function hideCondition($callback)
    {
        static::$cols['hide_condition'] = $callback;
        static::putSingleton();
        return new static();
    }

    public function confirmation($title, $message)
    {
        static::$cols['confirmation'] = true;
        static::$cols['confirmation_title'] = $title;
        static::$cols['confirmation_text'] = $message;
        static::putSingleton();
        return new static();
    }

    public function buttonColor($color)
    {
        static::$cols['color'] = $color;
        static::putSingleton();
        return new static();
    }

    public function moduleURL($path)
    {
        static::$cols['url'] = CRUDBooster::moduleURL($path);
        static::putSingleton();
        return new static();
    }

    public function javascript($url)
    {
        static::$cols['url'] = "javascript:".$url.";";
        static::putSingleton();
        return new static();
    }

    public function action($actionName)
    {
        static::$cols['url'] = action($actionName);
        static::putSingleton();
        return new static();
    }

    public function route($routeName)
    {
        static::$cols['url'] = action($routeName);
        static::putSingleton();
        return new static();
    }

    public function url($url)
    {
        static::$cols['url'] = $url;
        static::putSingleton();
        return new static();
    }

}