<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/19/2018
 * Time: 8:17 PM
 */

namespace crocodicstudio\crudbooster\scaffold;


class ColumnsSingleton
{

    private $columns = [];
    private $forms = [];
    private $addAction = [];

    public function addColumn($name, $data) {
        $this->columns[$name] = $data;
    }

    public function getColumn() {
        return $this->columns;
    }

    public function addForm($name, $data) {
        $this->forms[$name] = $data;
    }

    public function getForm() {
        return $this->forms;
    }


    public function addAddAction($name, $data) {
        $this->addAction[$name] = $data;
    }

    public function getAddAction() {
        return $this->addAction;
    }

}