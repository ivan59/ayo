<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21/09/18
 * Time: 08.40
 */

namespace App\Http\Controllers\Apis;

use App\Repos\CmsUsers;
use App\Repos\Mutations;
use App\Repos\Outlets;
use Illuminate\Support\Facades\Cache;
use App\Repos\Users;

class SessionID
{

    private $sessionID = null;
    private $sid = null;
    private $actingData = null;

    public function __construct()
    {
        $this->sid = request()->header('sid');
    }

    public function put($sessionID)
    {
        $this->sessionID = $sessionID;
    }

    public function getSessionID()
    {
        return $this->sid;
    }

    public function get($key = null)
    {
        if($this->actingData) {
            $data = $this->actingData;
        }else{
            $data = Cache::get($this->sid);    
            $this->refreshSession($data);
        }
                
        if($key) {
            return $data[$key];
        }else{

            $sess = new SessionModel();
            $sess->setId($data['id']);
            $sess->setName($data['name']);
            $sess->setMutationsCode($data['mutations_code']);
            $sess->setCmsPrivilegesId($data['cms_privileges_id']);
            $sess->setCmsPrivilegesName($data['cms_privileges_name']);
            $sess->setMutationsId($data['mutations_id']);
            $sess->setIp($data['ip']);
            $sess->setOutletId($data['outlet_id']);
            $sess->setOutletName($data['outlet_name']);
            $sess->setSuppliersId($data['suppliers_id']);
            $sess->setSuppliersName($data['suppliers_name']);
            $sess->setUseragent($data['useragent']);
            return $sess;
        }
    }

    public function guard()
    {
        if(!$this->sid || !Cache::has($this->sid)) {
            // $this->sessionIDExpired();
            return false;
        }else{
            return true;
        }
    }

    private function sessionIDExpired()
    {
        return response()->json(['api_status'=>0,'api_message'=>'SESSION_EXPIRED']);            
    }

    private function refreshSession($data)
    {
        Cache::put($this->sid,$data, 3600*4);
    }

    public function generate(CmsUsers $user )
    {
        $sessionID = ($this->sid)?:str_random(6);

        $outlet = Outlets::simpleQuery()->where("cms_users_id",$user->getId())->first();

        Cache::put($sessionID,[
            'id'=>$user->getId(),
            'name'=>$user->getName(),
            'outlet_id'=>$outlet->id,
            'outlet_name'=>$outlet->outlet_name,
            'suppliers_id'=>$user->getSuppliers()->getId(),
            'suppliers_name'=>$user->getSuppliers()->getCompanyName(),
            'mutations_id'=> $user->getMutations()->getId(),
            'mutations_code'=>$user->getMutations()->getCode(),
            'cms_privileges_name'=>$user->getCmsPrivileges()->getName(),
            'cms_privileges_id'=>$user->getCmsPrivileges()->getId(),
            'ip'=>request()->ip(),
            'useragent'=>request()->userAgent()
        ], 3600*4);
        return $sessionID;
    }

    public function invalidate()
    {
        Cache::forget($this->sid);
    }
}