<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use App\Services\SchedulesService;
		use crocodicstudio\crudbooster\helpers\CRUDBooster;
        use crocodicstudio\crudbooster\helpers\CB;
		use crocodicstudio\crudbooster\controllers\ApiController;

		class ApiListReportsController extends ApiController {

		    function __construct() {    
				$this->table       = "schedules";        
				$this->permalink   = "list_reports";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		    	$limit = $postdata['limit'];
		    	$offset = $postdata['offset'];

                $queryData = SchedulesService::listReports($limit,$offset);

                $resp = [];
                $resp['api_status']  = 1;
                $resp['api_message'] = "success";
                $resp['data']        = $queryData;
                $this->output($resp);

		    }

		}