<?php 
namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\scaffold\AddAction;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\scaffold\Columns as Col;

class AdminTeamsController extends CBController {

    public function cbInit() {
        
        $this->table 			   = "teams";	        
        $this->title_field         = "team_name";
        $this->limit               = 20;
        $this->orderby             = ["id","desc"];
        $this->show_numbering      = FALSE;     
        $this->button_table_action = TRUE;   
        $this->button_action_style = "button_icon";     
        $this->button_add          = TRUE;
        $this->button_delete       = TRUE;
        $this->button_edit         = TRUE;
        $this->button_detail       = TRUE;
        $this->button_show         = TRUE;
        $this->button_filter       = TRUE;        
        $this->button_export       = FALSE;	        
        $this->button_import       = FALSE;
        $this->button_bulk_action  = TRUE;	
        $this->auto_modal_form     = TRUE;
        $this->modal_form          = FALSE;
        $this->form_label_width    = 2;
        $this->form_input_width    = 9;
        $this->compact_form        = false;
        $this->compact_form_align  = "center";
        $this->sidebar_mode		    = "normal"; //normal,mini,collapse,collapse-mini							      
		Col::add("Team Name","team_name");
		Col::add("Team Logo","team_logo")->imageable();
		Col::add("Since","since");
		Col::add("Team Address","team_address");
		Col::add("Team City","team_city");

		$this->form = [];
		$this->form[] = ["label"=>"Team Name","name"=>"team_name","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Team Logo","name"=>"team_logo","type"=>"upload","help"=>"Recommended resolution is 200x200px","validation"=>"required|min:1|max:1000",'resize_width'=>90,'resize_height'=>90];
		$this->form[] = ["label"=>"Since","name"=>"since","type"=>"text","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Team Address","name"=>"team_address","type"=>"textarea","validation"=>"required|string|min:5|max:5000"];
		$this->form[] = ["label"=>"Team City","name"=>"team_city","type"=>"text","validation"=>"required|min:1|max:255"];

        AddAction::add("Players")
            ->icon("fa fa-users")
            ->buttonColor("primary")
            ->url('players?parent_id=[id]');

    }

    public function hook_query_index(&$query) {
        $query->whereNull('teams.deleted_at');
    }
}