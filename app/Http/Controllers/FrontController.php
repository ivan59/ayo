<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmails;
use App\Repos\Notification;
use App\Repos\Schedules;
use App\Repos\CmsUsers;
use App\Repos\Suppliers;
use App\Repos\Users;
use App\Repos\Leads;
use App\Repos\LeadsLog;
use App\Repos\MasterNotificationAuto;
use Illuminate\Http\Request;
use crocodicstudio\crudbooster\helpers\CB;
use DB;
use CRUDBooster;

class FrontController extends Controller
{
    public function getCronjob(){
        
        $leads = Leads::getLeadsActive();
        // dd($leads);
        foreach($leads as $l){
            // case timeout by status
            if($l->status_lead=='New Lead'){
                // before timeout
                $before_log         = LeadsLog::detailLog($l->id,'New Lead');
                $before_setting     = MasterNotificationAuto::firstStatus('Before New Lead');
                $repeat_new_lead    = MasterNotificationAuto::firstStatus('Repeat Reminder New Lead');
                // repeat
                $limit_repeat_new_lead = sumDate($before_log->created_at,$repeat_new_lead->time*3600);
                if($limit_repeat_new_lead<now('')){
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$repeat_new_lead->content);

                    $datas  = ['title'=>$repeat_new_lead->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$repeat_new_lead->title,$replace);
                }

                // before
                $limit_date_before = sumDate($before_log->created_at,$before_setting->time*3600);
                if($limit_date_before<now('')){
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$before_setting->content);

                    $datas  = ['title'=>$before_setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$before_setting->title,$replace);
                }

                //timeout
                $log = LeadsLog::detailLog($l->id,'New Lead');
                $setting = MasterNotificationAuto::firstStatus('New Lead');
                
                $limit_date = sumDate($log->created_at,$setting->time*3600);
                if($limit_date<now('')){
                    $up = Leads::findById($l->id);
                    $up->setIsTimeout(1);
                    $up->save();

                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$setting->content);

                    $datas  = ['title'=>$setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$setting->title,$replace);
                    Notification::saveNotification($sales->parent_cms_users,$setting->title,$replace);
                }
            }elseif($l->status_lead=='Cold'){
                // before timeout
                $before_log = LeadsLog::detailLog($l->id,'Status berpindah ke Cold');
                $before_setting = MasterNotificationAuto::firstStatus('Before Cold');
                $repeat_cold = MasterNotificationAuto::firstStatus('Repeat Reminder Cold');

                // repeat
                $limit_repeat_cold = sumDate($before_log->created_at,$repeat_cold->time*3600);
                if($limit_repeat_cold<now('')){
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$repeat_cold->content);

                    $datas  = ['title'=>$repeat_cold->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$repeat_cold->title,$replace);
                }

                // before
                $limit_date_before = sumDate($before_log->created_at,$before_setting->time*3600);
                if($limit_date_before<now('')){
                    
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$before_setting->content);

                    $datas  = ['title'=>$before_setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$before_setting->title,$replace);
                }

                // timeout
                $log = LeadsLog::detailLog($l->id,'Status berpindah ke Cold');
                $setting = MasterNotificationAuto::firstStatus('Cold');
                $limit_date = sumDate($log->created_at,$setting->time*3600);

                if($limit_date<now('')){
                    $up = Leads::findById($l->id);
                    $up->setIsTimeout(1);
                    $up->save();

                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$setting->content);

                    $datas  = ['title'=>$setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$setting->title,$replace);
                    Notification::saveNotification($sales->parent_cms_users,$setting->title,$replace);
                }
            }elseif($l->status_lead=='Warm'){
                // before timeout
                $before_log = LeadsLog::detailLog($l->id,'Status berpindah ke Warm');
                $before_setting = MasterNotificationAuto::firstStatus('Before Warm');
                $repeat_warm = MasterNotificationAuto::firstStatus('Repeat Reminder Warm');

                // repeat
                $limit_repeat_warm = sumDate($before_log->created_at,$repeat_warm->time*3600);
                if($limit_repeat_warm<now('')){
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$repeat_warm->content);

                    $datas  = ['title'=>$repeat_warm->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$repeat_warm->title,$replace);
                }

                // before
                $limit_date_before = sumDate($before_log->created_at,$before_setting->time*3600);
                if($limit_date_before<now('')){
                    
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$before_setting->content);

                    $datas  = ['title'=>$before_setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$before_setting->title,$replace);
                }

                // timeout
                $log = LeadsLog::detailLog($l->id,'Status berpindah ke Warm');
                $setting = MasterNotificationAuto::firstStatus('Warm');
                $limit_date = sumDate($log->created_at,$setting->time*3600);

                if($limit_date<now('')){
                    $up = Leads::findById($l->id);
                    $up->setIsTimeout(1);
                    $up->save();

                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$setting->content);

                    $datas  = ['title'=>$setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$setting->title,$replace);
                    Notification::saveNotification($sales->parent_cms_users,$setting->title,$replace);
                }
            }elseif($l->status_lead=='Hot'){
                // before timeout
                $before_log = LeadsLog::detailLog($l->id,'Status berpindah ke Hot');
                $before_setting = MasterNotificationAuto::firstStatus('Before Hot');
                $repeat_hot = MasterNotificationAuto::firstStatus('Repeat Reminder Hot');

                // repeat
                $limit_repeat_hot = sumDate($before_log->created_at,$repeat_hot->time*3600);
                if($limit_repeat_hot<now('')){
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$repeat_hot->content);

                    $datas  = ['title'=>$repeat_hot->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$repeat_hot->title,$replace);
                }

                // before   
                $limit_date_before = sumDate($before_log->created_at,$before_setting->time*3600);
                if($limit_date_before<now('')){
                    
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$before_setting->content);

                    $datas  = ['title'=>$before_setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$before_setting->title,$replace);
                }

                // timeout
                $log = LeadsLog::detailLog($l->id,'Status berpindah ke Hot');
                $setting = MasterNotificationAuto::firstStatus('Hot');
                $limit_date = sumDate($log->created_at,$setting->time*3600);

                if($limit_date<now('')){
                    $up = Leads::findById($l->id);
                    $up->setIsTimeout(1);
                    $up->save();

                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$setting->content);

                    $datas  = ['title'=>$setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$setting->title,$replace);
                    Notification::saveNotification($sales->parent_cms_users,$setting->title,$replace);
                }
            }elseif($l->status_lead=='Deal'){
                // before timeout
                $before_log = LeadsLog::detailLog($l->id,'Status berpindah ke Deal');
                $before_setting = MasterNotificationAuto::firstStatus('Before Deal');
                $repeat_deal = MasterNotificationAuto::firstStatus('Repeat Reminder Deal');

                // repeat
                $limit_repeat_deal = sumDate($before_log->created_at,$repeat_deal->time*3600);
                if($limit_repeat_deal<now('')){
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$repeat_deal->content);

                    $datas  = ['title'=>$repeat_deal->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$repeat_deal->title,$replace);
                }

                // before
                $limit_date_before = sumDate($before_log->created_at,$before_setting->time*3600);
                if($limit_date_before<now('')){
                    
                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$before_setting->content);

                    $datas  = ['title'=>$before_setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$before_setting->title,$replace);
                }

                // timeout
                $log = LeadsLog::detailLog($l->id,'Status berpindah ke Deal');
                $setting = MasterNotificationAuto::firstStatus('Deal');
                $limit_date = sumDate($log->created_at,$setting->time*3600);

                if($limit_date<now('')){
                    $up = Leads::findById($l->id);
                    $up->setIsTimeout(1);
                    $up->save();

                    $sales = CmsUsers::getFirstData($l->id_cms_users);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $replace = str_replace("nama_lead",$l->first_name.' '.$l->family_name,$setting->content);

                    $datas  = ['title'=>$setting->title,'content'=>$replace,"type"=>"overlead_timeout","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($l->id_cms_users,$setting->title,$replace);
                    Notification::saveNotification($sales->parent_cms_users,$setting->title,$replace);
                }
            }
        }

        // case reminder jadwal
        $jadwal = Schedules::listReminder();
        if(count($jadwal)!=0){
            foreach($jadwal as $j){
                // before one hour
                $setting = MasterNotificationAuto::firstStatus('Before Reminder Schedule');
                $limit_date = sumDate($j->date,$setting->time*3600);

                if($limit_date<now('')){
                    $sales = CmsUsers::getDataSales($j->id_leads);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $lead = Leads::firstData($j->id_leads);

                    $replace = str_replace("type",$j->type,$setting->content);
                    $replacex = str_replace("nama_lead",$lead->first_name.' '.$lead->family_name,$replace);
                    $replacexx = str_replace("tempat",$j->place,$replacex);
                    $replacexxx = str_replace("waktu",date('d-m-Y', strtotime($j->date)),$replacexx);

                    $datas  = ['title'=>$setting->title,'content'=>$replacexxx,"type"=>"mylead_schedule","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($sales->id,$setting->title,$replacexxx);
                    Notification::saveNotification($sales_manager->id,$setting->title,$replacexxx);
                }

                // timeout
                $setting_end = MasterNotificationAuto::firstStatus('Reminder Schedule');
                $limit_date_end = sumDate($j->date,$setting_end->time*3600);

                if($limit_date_end<now('')){
                    $sales = CmsUsers::getDataSales($j->id_leads);
                    $sales_manager = CmsUsers::getDataSalesManager($sales->parent_cms_users);
                    $regID = array($sales->regid,$sales_manager->regid);

                    $lead = Leads::firstData($j->id_leads);
                    
                    $freplace = str_replace("type",$j->type,$setting_end->content);
                    $freplacex = str_replace("nama_lead",$lead->first_name.' '.$lead->family_name,$freplace);
                    $freplacexx = str_replace("tempat",$j->place,$freplacex);
                    $freplacexxx = str_replace("waktu",date('d-m-Y', strtotime($j->date)),$freplacexx);

                    $datas  = ['title'=>$setting_end->title,'content'=>$freplacexxx,"type"=>"mylead_schedule","data"=>""];
                    $tes = CRUDBooster::sendFCM($regID,$datas);
                    Notification::saveNotification($sales->id,$setting_end->title,$freplacexxx);
                    Notification::saveNotification($sales_manager->id,$setting_end->title,$freplacexxx);

                    $upda['is_hit'] = 1;
                    DB::table('schedules')->where('id',$j->id)->update($upda);

                }      
            }
        }

    	echo 'Berhasil hit cronjob..............';
    }

    public function getTest() {
        $emailJob = new SendEmails();
        // dispatch($emailJob);
        \Queue::push(new SendEmails());
    }
}