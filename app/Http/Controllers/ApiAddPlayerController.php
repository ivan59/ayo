<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use App\Repos\Players;
		use crocodicstudio\crudbooster\helpers\CRUDBooster;
        use crocodicstudio\crudbooster\helpers\CB;
		use crocodicstudio\crudbooster\controllers\ApiController;

		class ApiAddPlayerController extends ApiController {

		    function __construct() {    
				$this->table       = "players";        
				$this->permalink   = "add_player";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		    	$teamsId = g('teams_id');
		        $backNumber = g('back_number');

		        $checkBackNumberInTeam = Players::checkBackNumber($teamsId,$backNumber);

		        if($checkBackNumberInTeam) {
		            $resp = [];
	                $resp['api_status']  = 0;
	                $resp['api_message'] = "The back number cannot be the same in one team";
	                $this->output($resp);
		        }
		    }

            /**
             * @param $query \Illuminate\Database\Query\Builder
             */
		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}