<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use crocodicstudio\crudbooster\helpers\CRUDBooster;
        use crocodicstudio\crudbooster\helpers\CB;
		use crocodicstudio\crudbooster\controllers\ApiController;

		class ApiEditTeamController extends ApiController {

		    function __construct() {    
				$this->table       = "teams";        
				$this->permalink   = "edit_team";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

            /**
             * @param $query \Illuminate\Database\Query\Builder
             */
		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}