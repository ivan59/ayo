<?php 
namespace App\Http\Controllers;

use App\Repos\Players;
use App\Repos\Teams;
use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\scaffold\Columns as Col;

class AdminPlayersController extends CBController {

    public function cbInit() {
        
        $this->table 			   = "players";	        
        $this->title_field         = "name";
        $this->limit               = 20;
        $this->orderby             = ["id","desc"];
        $this->show_numbering      = FALSE;     
        $this->button_table_action = TRUE;   
        $this->button_action_style = "button_icon";     
        $this->button_add          = TRUE;
        $this->button_delete       = TRUE;
        $this->button_edit         = TRUE;
        $this->button_detail       = TRUE;
        $this->button_show         = TRUE;
        $this->button_filter       = TRUE;        
        $this->button_export       = TRUE;	        
        $this->button_import       = FALSE;
        $this->button_bulk_action  = TRUE;	
        $this->auto_modal_form     = TRUE;
        $this->modal_form          = FALSE;
        $this->form_label_width    = 2;
        $this->form_input_width    = 9;
        $this->compact_form        = false;
        $this->compact_form_align  = "center";
        $this->sidebar_mode		   = "normal"; //normal,mini,collapse,collapse-mini							      
		Col::add("Name","name");
		Col::add("Height (cm)","height");
		Col::add("Weight (kg)","weight");
		Col::add("Position","position");
		Col::add("Back Number","back_number");

		$this->form = [];
		$this->form[] = ["label"=>"Name","name"=>"name","type"=>"text","validation"=>"required|string"];
		$this->form[] = ["label"=>"Height (cm)","name"=>"height","type"=>"number","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Weight (kg)","name"=>"weight","type"=>"number","validation"=>"required|min:1|max:255"];
		$this->form[] = ["label"=>"Position","name"=>"position","type"=>"select2","validation"=>"required","dataenum"=>array("penyerang","gelandang","bertahan","penjaga gawang")];
        $this->form[] = ["label"=>"Back Number","name"=>"back_number","type"=>"text","validation"=>"required|min:1|max:255"];
        $teams_id = '<input type="hidden" name="teams_id" value="'.g('parent_id').'">';
        $this->form[] = ['label' => '', 'type' => 'custom','name'=>'teams_id','html'=>$teams_id];

        $data['url_data']   = url('/admin/teams');
        $data['team'] = Teams::findById(g('parent_id'));
        $data['type'] = 'player';
        $this->pre_index_html = view("backend.header.header_table",$data)->render();
    }

    public function hook_query_index(&$query) {
        $parentId = g('parent_id');
        if($parentId) {
          $query->where('teams_id',$parentId)->orderBy('players.id','desc');
        }
        $query->whereNull('players.deleted_at');
    }

    public function hook_before_add(&$arr) {
        $teamsId = g('teams_id');
        $backNumber = g('back_number');

        $checkBackNumberInTeam = Players::checkBackNumber($teamsId,$backNumber);

        if($checkBackNumberInTeam) {
            CB::redirectBack("The back number cannot be the same in one team");
        }
    }

    public function hook_before_edit(&$arr, $id) {
        $teamsId = g('teams_id');
        $backNumber = g('back_number');

        $checkBackNumberInTeam = Players::checkBackNumber($teamsId,$backNumber,$id);

        if($checkBackNumberInTeam) {
            CB::redirectBack("The back number cannot be the same in one team");
        }
    }
}