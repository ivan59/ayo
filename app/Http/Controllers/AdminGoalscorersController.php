<?php 
namespace App\Http\Controllers;

use App\Repos\Teams;
use App\Repos\Schedules;
use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\scaffold\Columns as Col;

class AdminGoalscorersController extends CBController {

    public function cbInit() {
        
        $this->table 			   = "goalscorers";	        
        $this->title_field         = "id";
        $this->limit               = 20;
        $this->orderby             = ["id","desc"];
        $this->show_numbering      = FALSE;     
        $this->button_table_action = TRUE;   
        $this->button_action_style = "button_icon";     
        $this->button_add          = TRUE;
        $this->button_delete       = TRUE;
        $this->button_edit         = TRUE;
        $this->button_detail       = TRUE;
        $this->button_show         = TRUE;
        $this->button_filter       = TRUE;        
        $this->button_export       = FALSE;	        
        $this->button_import       = FALSE;
        $this->button_bulk_action  = TRUE;	
        $this->auto_modal_form     = FALSE;
        $this->modal_form          = FALSE;
        $this->form_label_width    = 2;
        $this->form_input_width    = 9;
        $this->compact_form        = false;
        $this->compact_form_align  = "center";
        $this->sidebar_mode		    = "normal"; //normal,mini,collapse,collapse-mini							      
		Col::add("Player","name")->fetchFrom("players","players_id");
		Col::add("Goal Time","goal_time");

		$this->form = [];
		$this->form[] = ["label"=>"Player","name"=>"players_id","type"=>"select2","validation"=>"required|integer|min:0","datatable"=>"players,name"];
		$this->form[] = ["label"=>"Goal Time","name"=>"goal_time","type"=>"text","validation"=>"required","placeholder"=>"example : 12:20"];
        $schedules_id = '<input type="hidden" name="schedules_id" value="'.g('parent_id').'">';
        $this->form[] = ['label' => '', 'type' => 'custom','name'=>'schedules_id','html'=>$schedules_id];
        $teams_id = '<input type="hidden" name="teams_id" value="'.g('parent_field').'">';
        $this->form[] = ['label' => '', 'type' => 'custom','name'=>'teams_id','html'=>$teams_id];

        $data['url_data']    = url('/admin/schedules');
        $data['schedules']   = $schedules = Schedules::findById(g('parent_id'));
        $data['home_team']   = Teams::findByIdCustom($schedules->getHomeTeam())->team_name;
        $data['guest_team']  = Teams::findByIdCustom($schedules->getGuestTeam())->team_name;
        $data['type'] = 'goal scorer';
        $this->pre_index_html = view("backend.header.header_table",$data)->render();
    }

    public function hook_query_index(&$query) {
        $parentId = g('parent_id');
        $parentField = g('parent_field');
        
        $query->where('schedules_id',$parentId)->where('goalscorers.teams_id',$parentField)->orderBy('players.id','desc')->whereNull('goalscorers.deleted_at');
    }
}