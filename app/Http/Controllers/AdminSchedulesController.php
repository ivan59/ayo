<?php 
namespace App\Http\Controllers;

use App\Repos\Schedules;
use App\Repos\Teams;
use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\scaffold\AddAction;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\scaffold\Columns as Col;

class AdminSchedulesController extends CBController {

    public function cbInit() {
        
        $this->table 			   = "schedules";	        
        $this->title_field         = "id";
        $this->limit               = 20;
        $this->orderby             = ["id","desc"];
        $this->show_numbering      = FALSE;     
        $this->button_table_action = TRUE;   
        $this->button_action_style = "button_icon";     
        $this->button_add          = TRUE;
        $this->button_delete       = TRUE;
        $this->button_edit         = TRUE;
        $this->button_detail       = TRUE;
        $this->button_show         = TRUE;
        $this->button_filter       = TRUE;        
        $this->button_export       = FALSE;	        
        $this->button_import       = FALSE;
        $this->button_bulk_action  = TRUE;	
        $this->auto_modal_form     = TRUE;
        $this->modal_form          = FALSE;
        $this->form_label_width    = 2;
        $this->form_input_width    = 9;
        $this->compact_form        = false;
        $this->compact_form_align  = "center";
        $this->sidebar_mode		   = "collapse-mini"; //normal,mini,collapse,collapse-mini							         
        $this->col[] = ["label"=>"Home Team","name"=>"home_team","callback"=>function($row) { 
            return Teams::findByIdCustom($row->home_team)->team_name;
        }];
        $this->col[] = ["label"=>"Home Team Score","name"=>"home_team_goals","callback"=>function($row) { 
            return "<input type='number' class='form-control' value=".$row->home_team_goals." placeholder='number of goals' onchange='homeTeamGoals(this.value+-+$row->id)'>";
        }];
        $this->col[] = ["label"=>"Guest Team","name"=>"guest_team","callback"=>function($row) { 
            return Teams::findByIdCustom($row->guest_team)->team_name;
        }];
        $this->col[] = ["label"=>"Guest Team Score","name"=>"guest_team_goals","callback"=>function($row) { 
            return "<input type='number' class='form-control' value=".$row->guest_team_goals." placeholder='number of goals' onchange='guestTeamGoals(this.value+-+$row->id)'>";
        }];
        $this->col[] = ["label"=>"Match Date","name"=>"match_date"];
        $this->col[] = ["label"=>"Match Time","name"=>"match_time"];

		$this->form = [];
        $this->form[] = ["label"=>"Home Team","name"=>"home_team","type"=>"select2","validation"=>"required","datatable"=>"teams,team_name"];
        $this->form[] = ["label"=>"Guest Team","name"=>"guest_team","type"=>"select2","validation"=>"required","datatable"=>"teams,team_name"];
		$this->form[] = ["label"=>"Match Date","name"=>"match_date","type"=>"date","validation"=>"required|date"];
		$this->form[] = ["label"=>"Match Time","name"=>"match_time","type"=>"time","validation"=>"required|date_format:H:i:s"];

        AddAction::add("Home Team Goals")
            ->icon("fa fa-user")
            ->buttonColor("success")
            ->url('goalscorers?parent_id=[id]&parent_field=[home_team]');

            AddAction::add("Guest Team Goals")
            ->icon("fa fa-user")
            ->buttonColor("warning")
            ->url('goalscorers?parent_id=[id]&parent_field=[guest_team]');
    }

    public function hook_before_add(&$arr) {
        $getHomeTeam = g('home_team');
        $getGuestTeam = g('guest_team');

        if($getHomeTeam == $getGuestTeam){
            CB::redirectBack("can't use the same team");
        }
    }

    public function hook_before_edit(&$arr, $id) {
       $getHomeTeam = g('home_team');
        $getGuestTeam = g('guest_team');

        if($getHomeTeam == $getGuestTeam){
            CB::redirectBack("can't use the same team");
        }
    }

    public function postUpdateGoal() {
        $schedulesId = g('schedules_id');
        $goals = g('goals');
        $type = g('type');

        $update = Schedules::findById($schedulesId);
        if($type == 'home team') {
            $update->setHomeTeamGoals($goals);
        }else {
            $update->setGuestTeamGoals($goals);
        }
        $update->save();
    }

    public function hook_query_index(&$query) {
        $query->whereNull('schedules.deleted_at');
    }
}