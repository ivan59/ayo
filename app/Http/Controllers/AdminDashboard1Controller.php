<?php 
namespace App\Http\Controllers;

use App\Repos\Teams;
use App\Repos\Schedules;
use App\Repos\Players;
use App\Repos\Goalscorers;
use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\helpers\CB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\scaffold\Columns as Col;

class AdminDashboard1Controller extends CBController {

    public function cbInit() {
        
        $this->table 			   = "api_logs";	        
        $this->title_field         = "name";
        $this->limit               = 20;
        $this->orderby             = ["id","desc"];
        $this->show_numbering      = FALSE;     
        $this->button_table_action = TRUE;   
        $this->button_action_style = "button_icon";     
        $this->button_add          = TRUE;
        $this->button_delete       = TRUE;
        $this->button_edit         = TRUE;
        $this->button_detail       = TRUE;
        $this->button_show         = TRUE;
        $this->button_filter       = TRUE;        
        $this->button_export       = FALSE;	        
        $this->button_import       = FALSE;
        $this->button_bulk_action  = TRUE;	
        $this->auto_modal_form     = TRUE;
        $this->modal_form          = FALSE;
        $this->form_label_width    = 2;
        $this->form_input_width    = 9;
        $this->compact_form        = false;
        $this->compact_form_align  = "center";
        $this->sidebar_mode		    = "normal"; //normal,mini,collapse,collapse-mini							      
    }

    public function getIndex() {
        $data['countTeams'] = Teams::countData();
        $data['countSchedules'] = Schedules::countData();
        $data['countPlayers'] = Players::countData();
        $data['schedulesToday'] = Schedules::getSchedulesToday();
        $data['topScorer'] = Goalscorers::getTopScorer();
        $data['no'] = 1;
        return view('backend.dashboard.dashboard', $data);
    }
}