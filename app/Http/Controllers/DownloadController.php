<?php

namespace App\Http\Controllers;

use App\Repos\CourierTaskOrders;
use App\Repos\OrderDetails;
use App\Repos\OrderHistoryPayments;
use App\Repos\Orders;
use App\Repos\VisitPlanOutletDates;
use crocodicstudio\crudbooster\helpers\CB;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function getInvoiceSales($visit_plan_outlet_dates_id, $filename)
    {
        $visitPlanOutletDates = VisitPlanOutletDates::findById($visit_plan_outlet_dates_id);

        $data = [];
        $data['visit'] = $visitPlanOutletDates;
        $data['supplier'] = $visitPlanOutletDates->getVisitPlanOutlets()->getVisitPlans()->getMutations()->getCmsUsers()->getSuppliers();
        $data['customer'] = $visitPlanOutletDates->getVisitPlanOutlets()->getOutlets();
        $data['order'] = Orders::findByVisitPlanOutletDates($visitPlanOutletDates->getId());
        $data['order_details'] = OrderDetails::findAllByOrderId($data['order']->getId());
        $view = view('backend.invoices.export_invoice_sales', $data)->render();
        return CB::exportPDF("Nota Penjualan ".date('d-m-Y'),$view,"A4");
    }

    public function getInvoiceBillFromCourier($courier_task_orders_id, $filename) {
        $courierTaskOrders = CourierTaskOrders::findById($courier_task_orders_id);
        $data = [];
        $data['task'] = $courierTaskOrders;
        $data['supplier'] = $courierTaskOrders->getOrders()->getSuppliers();
        $data['customer'] = $courierTaskOrders->getOrders()->getOutlets();
        $data['bills'] = OrderHistoryPayments::findAllByCourierTaskOrders($courier_task_orders_id);
        $view = view('backend.invoices.export_invoice_bill', $data)->render();
        return CB::exportPDF("Nota Penagihan ".date('d-m-Y'),$view,"A4");
    }

    public function getInvoiceBill($visit_plan_outlet_dates_id, $filename)
    {
        $visitPlanOutletDates = VisitPlanOutletDates::findById($visit_plan_outlet_dates_id);

        $data = [];
        $data['visit'] = $visitPlanOutletDates;
        $data['supplier'] = $visitPlanOutletDates->getVisitPlanOutlets()->getVisitPlans()->getMutations()->getCmsUsers()->getSuppliers();
        $data['customer'] = $visitPlanOutletDates->getVisitPlanOutlets()->getOutlets();
        $data['bills'] = OrderHistoryPayments::findAllByVisitPlanOutletDates($visitPlanOutletDates->getId());
        $view = view('backend.invoices.export_invoice_bill', $data)->render();
        return CB::exportPDF("Nota Penagihan ".date('d-m-Y'),$view,"A4");
    }
    
    public function getInvoiceByOrder($orders_id, $filename)
    {
        $order = Orders::findById($orders_id);
        $data = [];
        $data['supplier'] = $order->getSuppliers();
        $data['customer'] = $order->getOutlets();
        $data['order'] = $order;
        $data['order_details'] = OrderDetails::findAllByOrderId($orders_id);
        $view = view('backend.invoices.export_invoice_sales', $data)->render();
        return CB::exportPDF("Nota Penjualan ".date('d-m-Y'),$view,"A4");
    }
}
