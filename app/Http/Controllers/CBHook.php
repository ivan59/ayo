<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Apis\SessionID;
use App\Repos\CmsUsers;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use DB;
use Illuminate\Http\Response;
use Session;
use Request;
use crocodicstudio\crudbooster\helpers\CB;

class CBHook extends Controller {

	public static function afterLogin() {
	    $user = CmsUsers::findById(CB::myId());
	    session(['suppliers_id'=>$user->getSuppliers()->getId()]);
	}

	public static function beforeDashboard()
    {
        if(CRUDBooster::myPrivilegeName() == "Admin Supplier") {
            redirect()->action("AdminDashboardController@getSalesSummary")->send();
            exit();
        }
    }

	public static function beforeBackendMiddleware()
    {
        $user = CmsUsers::findById(CB::myId());

        if($user->getCmsPrivileges()->getName() == "Admin Supplier") {
            if($user->getSuppliers()->getStatus() == "Tidak Aktif" || $user->getSuppliers()->getIsApproved() != 1) {
                session()->flush();
                CB::redirect(config('crudbooster.ADMIN_PATH').'/login',"Maaf akun Anda saat ini tidak aktif, silahkan hubungi Admin","warning");
            }
        }
    }

    public static function beforeAuthAPIMiddleware($request)
    {

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Response $response
     * @param $duration
     */
    public static function afterAuthAPIMiddleware($request, $response, $duration)
    {
        if($response->getStatusCode() != 200) {
            $responseData = $response->getOriginalContent();
        }else{
            $responseData = $response->getContent();
        }

        $requestData = $request->all();

        if(config('app.api_db_log')) {
            \DB::table("api_logs")->insert([
                'created_at'=>date('Y-m-d H:i:s'),
                "useragent"=>$request->header("User-Agent"),
                'name'=>basename($request->url()),
                'url'=>$request->fullUrl(),
                "duration"=> $duration,
                "ip"=> $request->getClientIp(),
                "sid_data"=> json_encode((new SessionID())->get()),
                "request_header"=> json_encode($request->headers->all()),
                "request_method"=> $request->getMethod(),
                'request_data'=> json_encode($requestData),
                'response_data'=>$responseData,
                'status'=> $response->getStatusCode()
            ]);
        }

    }
}