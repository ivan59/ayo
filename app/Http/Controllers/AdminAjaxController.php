<?php

namespace App\Http\Controllers;

use App\Repos\Areas;
use App\Repos\Cities;
use App\Repos\CmsUsers;
use App\Repos\Territories;
use Illuminate\Http\Request;

class AdminAjaxController extends Controller
{
    public function getAllSalesByTerritory($territories_id)
    {
        return response()->json(CmsUsers::findAllSalesAndSMByTerritory($territories_id));
    }
    
    public function getAllTerritoryByArea($areas_id)
    {
        return response()->json(Territories::findAllByArea($areas_id));
    }
    
    public function getAllAreaByRegion($regions_id)
    {
        return response()->json(Areas::findAllByRegion($regions_id));
    }
    
    public function getAllCityByProvince($provinces_id)
    {
        return response()->json(Cities::findAllByProvince($provinces_id));
    }
}