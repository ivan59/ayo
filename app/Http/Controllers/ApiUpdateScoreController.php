<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use App\Services\MatchResultStatusService;
		use crocodicstudio\crudbooster\helpers\CRUDBooster;
        use crocodicstudio\crudbooster\helpers\CB;
		use crocodicstudio\crudbooster\controllers\ApiController;

		class ApiUpdateScoreController extends ApiController {

		    function __construct() {    
				$this->table       = "schedules";        
				$this->permalink   = "update_score";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
		    	$schedulesId = $postdata['id'];
		    	$homeTeamGoals = $postdata['home_team_goals'];
		    	$guestTeamGoals = $postdata['guest_team_goals'];
				// remove if already data
		    	MatchResultStatusService::removeAlreadyData($schedulesId);

		    	// insert
		    	MatchResultStatusService::insertData($schedulesId,$homeTeamGoals,$guestTeamGoals);
		    }	

            /**
             * @param $query \Illuminate\Database\Query\Builder
             */
		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}