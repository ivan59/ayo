<?php

namespace App\Http\Controllers;

use App\Repos\Leads;
use App\Repos\LeadsLog;
use App\Repos\Project;
use App\Repos\CmsUsers;
use App\Http\Controllers\Controller;
use Session;
use Request;
use DB;
use CRUDBooster;

class DashboardController extends Controller
{
	public function getIndex(){
        // Count Data
        $data['page_title'] = "Dashboard";
        $data['leads_total'] = Leads::count();
        $data['project_total'] = Project::count();
        $data['salesm_total'] = CmsUsers::countSalesManagement();
        $data['sales_total'] = CmsUsers::countSales();

        // Stats Register
        $data['all_month'] = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember',
        ];

        $data['all_year'] = Leads::listYears();

        $tahun = date('Y');
        $bulan = date('m');

        for($i=1; $i <= 31 ; $i++) {
            $stats_register = Leads::statsRegister($tahun,$bulan,$i);

            if(empty($stats_register)) {
                $tgl = date('Y-m').'-'.$i;
                $obj = json_decode(json_encode(["date"=>$tgl,"jumlah"=>"0"]));
                $stats_per_month[] = $obj;
            }else {
                $stats_per_month[] = $stats_register;
            }
        }

        $data['stats_per_month'] = $stats_per_month;

        // Status Lead
        $lead_new = Leads::countLead('New Lead',$tahun,$bulan);
        $lead_cold = Leads::countLead('Cold',$tahun,$bulan);
        $lead_warm = Leads::countLead('Warm',$tahun,$bulan);
        $lead_hot = Leads::countLead('Hot',$tahun,$bulan);
        $lead_deal = Leads::countLead('Deal',$tahun,$bulan);

        $dataarray = array($lead_new,$lead_cold,$lead_warm,$lead_hot,$lead_deal);

        foreach ($dataarray as $da) {

            if ($da == NULL) {
                $da = 0;
            }
            
            $obj = json_decode(json_encode(["count"=>$da]));
            $statusleads[] = $obj;
        }

        $data['statusleads'] = $statusleads;

        // Leads Logs
        $data['leads_log'] = LeadsLog::dashboard();

        $popularproject = DB::table('leads')
        ->select('project.title', DB::raw("COUNT(leads.id_project) as total"))
        ->join('project','leads.id_project','=','project.id')
        ->orderBy('total','desc')
        ->limit(5)
        ->get();

        $data['popularproject'] = $popularproject;

        $popularsales = DB::table('leads')
        ->select('cms_users.name', DB::raw("COUNT(leads.id_cms_users) as total"))
        ->join('cms_users','leads.id_cms_users','=','cms_users.id')
        ->orderBy('total','desc')
        ->limit(5)
        ->get();

        $data['popularsales'] = $popularsales;

        // return view('custom.dashboard',$data);
        return redirect('admin/dashboard');
    }

    public function getStatsregister() {
        if (empty(g('bulan'))) {
            $bulan = date('m');
            $tahun = date('Y');
        }else{
            $bulan = g('bulan');
            $tahun = g('tahun');
        }
        for($i=1; $i <= 31 ; $i++) {
            $stats_register = Leads::statsRegister($tahun,$bulan,$i);

            if(empty($stats_register)) {
                $tgl = date('Y-m').'-'.$i;
                $obj =["date"=>$tgl,"jumlah"=>"0"];
                $stats_per_month[] = $obj;
            }else {
                $stats_per_month[] = $stats_register;
            }
        }

        $data['stats_per_month'] = $stats_per_month;

        return response($data);
    }

    public function getStatusleads() {
        if (empty(g('bulan'))) {
            $bulan = date('m');
            $tahun = date('Y');
        }else{
            $bulan = g('bulan');
            $tahun = g('tahun');
        }

        $lead_new = Leads::countLead('New Lead',$tahun,$bulan);
        $lead_cold = Leads::countLead('Cold',$tahun,$bulan);
        $lead_warm = Leads::countLead('Warm',$tahun,$bulan);
        $lead_hot = Leads::countLead('Hot',$tahun,$bulan);
        $lead_deal = Leads::countLead('Deal',$tahun,$bulan);

        $dataarray = array($lead_new,$lead_cold,$lead_warm,$lead_hot,$lead_deal);

        foreach ($dataarray as $da) {
            $obj = json_decode(json_encode(["count"=>$da]));
            $statusleads[] = $obj;
        }


        $data['statusleads'] = $statusleads;

        return response($data);
    }
}
