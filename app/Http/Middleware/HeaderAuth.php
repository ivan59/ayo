<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21/09/18
 * Time: 00.36
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class HeaderAuth
{
    private $secretKey = 'cr0c0d1c';

    public function handle($request, Closure $next)
    {
        if($request->get('token')) {
            $iv = str_random(5);
            $token = base64_encode(md5($this->secretKey.'-'.$iv).'.'.$iv);
            return response()->json(['token'=>$token]);
        }


        if(!$request->headers->has('Authorization')) {
            $errorCode = str_random(5);
            Log::warning("ERROR CODE (".$errorCode."): Authorization header not found");
            return response()->json(['status'=>false,'message'=>'Access Denied! ErrCode: '.$errorCode], 401);
        }

        $token = $request->header('Authorization');
        $token = str_replace("Bearer ","",$token);
        $token = base64_decode($token);
        $parseArray = explode('.', $token);
        $randomString = $parseArray[1];
        $hashToken = $parseArray[0];
        $serverToken = md5( $this->secretKey.'-'.$randomString );


        if($tempToken = Cache::get($serverToken)) {
            if($tempToken['ip']!=$request->ip() || $tempToken['useragent']!=$request->userAgent() || $tempToken['request_uri']!=$request->fullUrl()) {
                $errorCode = str_random(5);
                Log::warning("ERROR CODE (".$errorCode."): The token has already exists but unknown device request or invalid request uri");
                return response()->json(['status'=>false,'message'=>'Access Denied! ErrCode: '.$errorCode], 401);
            }

            if($tempToken['attempt']>=3) {
                $errorCode = str_random(5);
                Log::warning("ERROR CODE (".$errorCode."): User request token more than 3 times");
                return response()->json(['status'=>false,'message'=>'Access Denied! ErrCode: '.$errorCode], 401);
            }

            Cache::put($serverToken,[
                'ip'=>$tempToken['ip'],
                'request_uri'=>$tempToken['request_uri'],
                'useragent'=>$tempToken['useragent'],
                'attempt'=>$tempToken['attempt']+1
            ],1800);
        }

        if($hashToken!=$serverToken) {
            $errorCode = str_random(5);
            Log::warning("ERROR CODE (".$errorCode."): Request token is not equal with the server token");
            return response()->json(['status'=>false,'message'=>'Access Denied! ErrCode: '.$errorCode], 401);
        }

        if(!isset($tempToken)) {
            Cache::put($serverToken,[
                'ip'=>$request->ip(),
                'request_uri'=>$request->fullUrl(),
                'useragent'=>$request->userAgent(),
                'attempt'=>1
            ],1800);
        }

        return $next($request);
    }
}