<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21/09/18
 * Time: 10.32
 */

namespace App\Http\Repositories;

use Illuminate\Support\Facades\DB;

class UserRepo
{

    public static function table()
    {
        return DB::table("cms_users");
    }
    public static function baseQuery()
    {
        return static::table()
            ->join('cms_privileges','cms_privileges.id','=','id_cms_privileges')
            ->select('cms_users.*',
                'cms_privileges.id as role_id',
                DB::raw("IF(cms_users.photo NOT NULL,CONCAT('".asset('/')."',cms_users.photo),'') as photo"),
                'cms_privileges.name as role_name');
    }

    public static function findById(int $id)
    {
        return static::baseQuery()->where('id',$id)->first();
    }

    public static function findByEmail(String $email)
    {
        return static::baseQuery()->where("email", $email)->first();
    }

    public static function save()
    {
        if(func_num_args() == 2) {
            $id = func_get_arg(0);
            $data = func_get_arg(1);
            return static::table()->where("id",$id)->update($data);
        }elseif (func_num_args() == 1){
            $data = func_get_arg(0);
            return static::table()->insert($data);
        }
        return false;
    }
}