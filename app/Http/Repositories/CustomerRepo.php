<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 21/09/18
 * Time: 10.32
 */

namespace App\Http\Repositories;

use Illuminate\Support\Facades\DB;

class CustomerRepo
{

    public static function table()
    {
        return DB::table("customers");
    }
    public static function baseQuery()
    {
        return static::table()
            ->join("mutations","mutations.id","=","customers.mutations_id")
            ->join("cms_users","cms_users.id","=","mutations.cms_users_id")
            ->join("areas","areas.id","=","mutations.areas_id")
            ->join("cms_privileges","cms_privileges.id","=","mutations.cms_privileges_id")
            ->select("customers.*",
                "cms_users.name as user_name",
                "areas.name as area_name",
                "cms_privileges.name as role_name");
    }

    public static function findById(int $id)
    {
        return static::baseQuery()->where("id",$id)->first();
    }

    public static function save()
    {
        if(func_num_args() == 2) {
            $id = func_get_arg(0);
            $data = func_get_arg(1);
            return static::table()->where("id",$id)->update($data);
        }elseif (func_num_args() == 1){
            $data = func_get_arg(0);
            return static::table()->insert($data);
        }
        return false;
    }
}