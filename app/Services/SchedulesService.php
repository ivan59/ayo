<?php
namespace App\Services;
use App\Repos\Schedules;
use App\Repos\Teams;
use App\Repos\Goalscorers;
use App\Repos\MatchResultStatus;
use File;

class SchedulesService
{
    public static function listReports($limit,$offset) {
        $query = Schedules::simpleQuery()
        ->limit($limit)
        ->offset($offset)
        ->whereNull('schedules.deleted_at')
        ->get();

        if(count($query)!=0) {
            foreach($query as $item) {
                if($item->home_team_goals == $item->guest_team_goals) {
                    $match_result = 'Draw';
                }elseif($item->home_team_goals > $item->guest_team_goals) {
                    $match_result = 'Tim Home Menang';
                }elseif($item->home_team_goals < $item->guest_team_goals) {
                    $match_result = 'Tim Away Menang';
                }

                $item->home_team = SchedulesService::nameTeam($item->home_team);
                $item->guest_team = SchedulesService::nameTeam($item->guest_team);
                $item->match_result = $match_result;
                $item->top_scorer_player = SchedulesService::topScorer($item->id);
                $item->home_team_accumulated_total_wins = SchedulesService::accumulatedWin($item->home_team->id,$item->match_date);
                $item->guest_team_accumulated_total_wins = SchedulesService::accumulatedWin($item->guest_team->id,$item->match_date);;
            }
        }

        return $query;  
    }

    public static function nameTeam($teamId) {
        $url = asset('/');
        return Teams::simpleQuery()
        ->select("teams.*",\DB::raw("concat('$url',team_logo) as team_logo"))
        ->where('id',$teamId)
        ->first();
    }

    public static function topScorer($schedulesId) {
        $query = Goalscorers::simpleQuery()
        ->where('schedules_id',$schedulesId)
        ->join('players','players.id','=','goalscorers.players_id')
        ->select('players.name', \DB::raw('count(*) as total'))
        ->groupBy('players_id')
        ->orderBy('total','desc')
        ->first();

        return $query->name;
    }

    public static function accumulatedWin($teamId,$matchDate) {
        return MatchResultStatus::simpleQuery()
        ->join('schedules','schedules.id','=','match_result_status.schedules_id')
        ->whereNotNull('home_team_goals')
        ->where('teams_id',$teamId)
        ->where('status','win')
        ->where('match_date','<=',$matchDate)
        ->count();
    }

}