<?php
namespace App\Services;
use App\Repos\Schedules;
use App\Repos\MatchResultStatus;
use File;

class MatchResultStatusService
{
    public static function removeAlreadyData($schedulesId) {
        MatchResultStatus::simpleQuery()
        ->where('schedules_id',$schedulesId)
        ->delete();
    }

    public static function insertData($schedulesId,$homeTeamGoals,$guestTeamGoals) {
        $schedule = Schedules::simpleQuery()
        ->where('id',$schedulesId)
        ->first();

        // insert home team
        $insertHomeTeam['schedules_id'] = $schedulesId;
        $insertHomeTeam['teams_id'] = $schedule->home_team;

        if($homeTeamGoals == $guestTeamGoals) {
            $insertHomeTeam['status'] = 'draw';
        }elseif($homeTeamGoals > $guestTeamGoals) {
            $insertHomeTeam['status'] = 'win';
        }else{
            $insertHomeTeam['status'] = 'lost';
        }
        MatchResultStatus::simpleQuery()->insert($insertHomeTeam);

        // insert guest team
        $insertGuestTeam['schedules_id'] = $schedulesId;
        $insertGuestTeam['teams_id'] = $schedule->guest_team;

        if($homeTeamGoals == $guestTeamGoals) {
            $insertGuestTeam['status'] = 'draw';
        }elseif($homeTeamGoals < $guestTeamGoals) {
            $insertGuestTeam['status'] = 'win';
        }else{
            $insertGuestTeam['status'] = 'lost';
        }
        MatchResultStatus::simpleQuery()->insert($insertGuestTeam);
    }
}