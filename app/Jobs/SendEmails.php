<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $post = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $attendances = \DB::table('attendances')->get();

        foreach ($attendances as $item)
        {   
            $insert['cms_users_id'] = $item->cms_users_id;
            $insert['created_at'] = now();
            \DB::table('attendances_copy')->insert($insert);
        }
    }
}
