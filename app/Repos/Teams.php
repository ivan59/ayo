<?php 
namespace App\Repos;

use DB;
use CB;

class Teams extends BaseRepo
{
	public static $tableName = "teams";

    
	private $id;
	private $teamName;
	private $teamLogo;
	private $since;
	private $teamAddress;
	private $teamCity;
	private $createdAt;
	private $updatedAt;
	private $deletedAt;


    
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getTeamName() {
		return $this->teamName;
	}

	public function setTeamName($teamName) {
		$this->teamName = $teamName;
	}

	public function getTeamLogo() {
		return $this->teamLogo;
	}

	public function setTeamLogo($teamLogo) {
		$this->teamLogo = $teamLogo;
	}

	public function getSince() {
		return $this->since;
	}

	public function setSince($since) {
		$this->since = $since;
	}

	public function getTeamAddress() {
		return $this->teamAddress;
	}

	public function setTeamAddress($teamAddress) {
		$this->teamAddress = $teamAddress;
	}

	public function getTeamCity() {
		return $this->teamCity;
	}

	public function setTeamCity($teamCity) {
		$this->teamCity = $teamCity;
	}

	public function getCreatedAt() {
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;
	}

	public function getUpdatedAt() {
		return $this->updatedAt;
	}

	public function setUpdatedAt($updatedAt) {
		$this->updatedAt = $updatedAt;
	}

	public function getDeletedAt() {
		return $this->deletedAt;
	}

	public function setDeletedAt($deletedAt) {
		$this->deletedAt = $deletedAt;
	}


}