<?php 
namespace App\Repos;

use DB;
use CB;

class Players extends BaseRepo
{
	public static $tableName = "players";

    
	private $id;
	private $name;
	private $height;
	private $weight;
	private $position;
	private $backNumber;
	private $createdAt;
	private $updatedAt;
	private $deletedAt;
	private $teams;


    
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getHeight() {
		return $this->height;
	}

	public function setHeight($height) {
		$this->height = $height;
	}

	public function getWeight() {
		return $this->weight;
	}

	public function setWeight($weight) {
		$this->weight = $weight;
	}

	public function getPosition() {
		return $this->position;
	}

	public function setPosition($position) {
		$this->position = $position;
	}

	public function getBackNumber() {
		return $this->backNumber;
	}

	public function setBackNumber($backNumber) {
		$this->backNumber = $backNumber;
	}

	public function getCreatedAt() {
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;
	}

	public function getUpdatedAt() {
		return $this->updatedAt;
	}

	public function setUpdatedAt($updatedAt) {
		$this->updatedAt = $updatedAt;
	}

	public function getDeletedAt() {
		return $this->deletedAt;
	}

	public function setDeletedAt($deletedAt) {
		$this->deletedAt = $deletedAt;
	}

	/**
	* @return Teams
	*/
	public function getTeams() {
		return Teams::findById($this->teams);
	}

	public function setTeams($teams) {
		$this->teams = $teams;
	}

	// custom
	public static function checkBackNumber($teamsId,$backNumber,$id=null){
		$query = static::simpleQuery()->where('teams_id',$teamsId)->where('back_number',$backNumber)->whereNull('deleted_at');
		if($id) {
			$query->where('id','!=',$id);
		}
		$query = $query->first();
		return $query;
	}

	public static function countData(){
		return static::simpleQuery()->whereNull('deleted_at')->count();
	}

}