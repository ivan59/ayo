<?php 
namespace App\Repos;

use DB;
use CB;

class CmsUsers extends BaseRepo
{
	public static $tableName = "cms_users";

	private $id;
	private $cmsPrivileges;
	private $createdAt;
	private $updatedAt;
	private $name;
	private $photo;
	private $phone;
	private $address;
	private $email;
	private $password;
	private $nik;
	private $dateOfBirth;
	private $noNpwp;
	private $photoKtp;
	private $status;
	private $switchStatus;
	private $nikKtp;
	private $motherName;
	private $religion;
	private $child;
	private $gender;
	private $leaveQuota;
	private $remainingDaysOff;
	private $workStart;
	private $endOfProbation;
	private $education;
	private $noIjazah;
	private $bpjs;
	private $expired;
	private $position;
	private $remark;
	private $accountNumber;
	private $banks;
	private $salary;
	private $latitude;
	private $longitude;
	private $regid;
	private $isActive;
    
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	/**
	* @return CmsPrivileges
	*/
	
	public function getCmsPrivileges() {
		return CmsPrivileges::findById($this->cmsPrivileges);
	}

	public function setCmsPrivileges($cmsPrivileges) {
		$this->cmsPrivileges = $cmsPrivileges;
	}

	public function getCreatedAt() {
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;
	}

	public function getUpdatedAt() {
		return $this->updatedAt;
	}

	public function setUpdatedAt($updatedAt) {
		$this->updatedAt = $updatedAt;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getPhoto() {
		return $this->photo;
	}

	public function setPhoto($photo) {
		$this->photo = $photo;
	}

	public function getPhone() {
		return $this->phone;
	}

	public function setPhone($phone) {
		$this->phone = $phone;
	}

	public function getAddress() {
		return $this->address;
	}

	public function setAddress($address) {
		$this->address = $address;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

	public function getNik() {
		return $this->nik;
	}

	public function setNik($nik) {
		$this->nik = $nik;
	}

	public function getDateOfBirth() {
		return $this->dateOfBirth;
	}

	public function setDateOfBirth($dateOfBirth) {
		$this->dateOfBirth = $dateOfBirth;
	}

	public function getNoNpwp() {
		return $this->noNpwp;
	}

	public function setNoNpwp($noNpwp) {
		$this->noNpwp = $noNpwp;
	}

	public function getPhotoKtp() {
		return $this->photoKtp;
	}

	public function setPhotoKtp($photoKtp) {
		$this->photoKtp = $photoKtp;
	}

	public function getStatus() {
		return $this->status;
	}

	public function setStatus($status) {
		$this->status = $status;
	}

	public function getSwitchStatus() {
		return $this->switchStatus;
	}

	public function setSwitchStatus($switchStatus) {
		$this->switchStatus = $switchStatus;
	}

	public function getNikKtp() {
		return $this->nikKtp;
	}

	public function setNikKtp($nikKtp) {
		$this->nikKtp = $nikKtp;
	}

	public function getMotherName() {
		return $this->motherName;
	}

	public function setMotherName($motherName) {
		$this->motherName = $motherName;
	}

	public function getReligion() {
		return $this->religion;
	}

	public function setReligion($religion) {
		$this->religion = $religion;
	}

	public function getChild() {
		return $this->child;
	}

	public function setChild($child) {
		$this->child = $child;
	}

	public function getGender() {
		return $this->gender;
	}

	public function setGender($gender) {
		$this->gender = $gender;
	}

	public function getLeaveQuota() {
		return $this->leaveQuota;
	}

	public function setLeaveQuota($leaveQuota) {
		$this->leaveQuota = $leaveQuota;
	}

	public function getRemainingDaysOff() {
		return $this->remainingDaysOff;
	}

	public function setRemainingDaysOff($remainingDaysOff) {
		$this->remainingDaysOff = $remainingDaysOff;
	}

	public function getWorkStart() {
		return $this->workStart;
	}

	public function setWorkStart($workStart) {
		$this->workStart = $workStart;
	}

	public function getEndOfProbation() {
		return $this->endOfProbation;
	}

	public function setEndOfProbation($endOfProbation) {
		$this->endOfProbation = $endOfProbation;
	}

	public function getEducation() {
		return $this->education;
	}

	public function setEducation($education) {
		$this->education = $education;
	}

	public function getNoIjazah() {
		return $this->noIjazah;
	}

	public function setNoIjazah($noIjazah) {
		$this->noIjazah = $noIjazah;
	}

	public function getBpjs() {
		return $this->bpjs;
	}

	public function setBpjs($bpjs) {
		$this->bpjs = $bpjs;
	}

	public function getExpired() {
		return $this->expired;
	}

	public function setExpired($expired) {
		$this->expired = $expired;
	}

	public function getPosition() {
		return $this->position;
	}

	public function setPosition($position) {
		$this->position = $position;
	}

	public function getRemark() {
		return $this->remark;
	}

	public function setRemark($remark) {
		$this->remark = $remark;
	}

	public function getAccountNumber() {
		return $this->accountNumber;
	}

	public function setAccountNumber($accountNumber) {
		$this->accountNumber = $accountNumber;
	}

	/**
	* @return Banks
	*/
	public function getBanks(){
		return $this->banks;
	}

	public function setBanks($banks){
		$this->banks = $banks;
	}

	public function getSalary() {
		return $this->salary;
	}

	public function setSalary($salary) {
		$this->salary = $salary;
	}

	public function getLatitude() {
		return $this->latitude;
	}

	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}

	public function getLongitude() {
		return $this->longitude;
	}

	public function setLongitude($longitude) {
		$this->longitude = $longitude;
	}

	public function getRegid() {
		return $this->regid;
	}

	public function setRegid($regid) {
		$this->regid = $regid;
	}

	public function getIsActive(){
		return $this->isActive;
	}

	public function setIsActive($isActive){
		$this->isActive = $isActive;
	}

	public static function firstbyEmail($email){
	  return static::simpleQuery()->where("email",$email)->first();
	}

	public static function getUpdate($email,$regid){
	  $u['regid'] = $regid;
	  static::simpleQuery()->where("email",$email)->update($u);
	}

	public static function getFirstData($id){
	  	return static::simpleQuery()->where("id",$id)->first();
	}

	public static function getUpdatePassword($id,$data){
		$u['password'] = $data;
	  	static::simpleQuery()->where("id",$id)->update($u);
	}

	public static function getLogout($id){
		$u['regid'] = null;
	  	static::simpleQuery()->where("id",$id)->update($u);
	}

	public static function getPluck(){
		return static::simpleQuery()->where('id_cms_privileges','!=',1)->pluck('regid');
	}

	public static function getSinglePluck($id){
		return static::simpleQuery()->where('id',$id)->pluck('regid');
	}

	public static function getData(){
		return static::simpleQuery()
		->join('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')
		->select('cms_users.*','cms_privileges.name as name_priv')
		->where('id_cms_privileges','<>',1)->get();
	}

	public static function getDataWithoutSuperadmin(){
		return static::simpleQuery()
		->join('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')
		->where('id_cms_privileges','<>',1)
		->select('cms_users.*','cms_privileges.*','cms_users.id as id','cms_privileges.name as cp_name','cms_privileges.id as cp_id','cms_users.name as name')
		->get();
	}

	public static function listApproval(){
		return static::simpleQuery()->where('id_cms_privileges','!=',2)->get();
	}

	public static function getDataSearch($value){
		return static::simpleQuery()->where('id_cms_privileges','<>',1)->where('name', 'like', '%' . $value . '%')->get();
	}

	public static function getFirstLatLong($type){
		$data = static::simpleQuery()->where('id_cms_privileges','<>',1)->orderBy('id','desc')->whereNotNull('latitude')->first();
		if($type=='lat'){
			return $data->latitude;
		}else{
			return $data->longitude;
		}
	}

	public static function getDataExport($limit,$is_active){
		$data = static::simpleQuery()
        ->join('master_education','master_education.id','=','cms_users.master_education_id')
        ->join('master_status','master_status.id','=','cms_users.master_status_id')
        ->join('banks','banks.id','=','cms_users.banks_id')
        ->where('id_cms_privileges','!=',1)
        ->limit($limit);
        if($is_active){
        	$data->where('is_active',$is_active);
        }
        $data = $data->get();
        return $data;
	}

	public static function listEdit($used){
		return static::simpleQuery()
		// ->whereNotIn('cms_users.id',$used)
		->where('id_cms_privileges','!=',1)
		->join('cms_privileges','cms_privileges.id','=','cms_users.id_cms_privileges')
		->select('cms_privileges.name as name_priv','cms_users.name as name_cu','cms_users.id as id')
		->get();
	}
}