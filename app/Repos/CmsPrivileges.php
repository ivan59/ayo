<?php 
namespace App\Repos;

use DB;
use CB;
use Illuminate\Support\Collection;

class CmsPrivileges extends BaseRepo
{
	public static $tableName = "cms_privileges";

	private $id;
	private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
	private $isSuperadmin;
	private $themeColor;
	private $createdAt;

    /**
     * @return CmsPrivileges
     */
	public static function findByName($name)
    {
        $row = static::simpleQuery()->where("name",$name)->first();
        return new static($row);
    }

    public static function findByNames($names) {
        return static::simpleQuery()->whereIn("name",$names)->pluck('id')->toArray();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getisSuperadmin()
    {
        return $this->isSuperadmin;
    }

    /**
     * @param mixed $isSuperadmin
     */
    public function setIsSuperadmin($isSuperadmin)
    {
        $this->isSuperadmin = $isSuperadmin;
    }

    /**
     * @return mixed
     */
    public function getThemeColor()
    {
        return $this->themeColor;
    }

    /**
     * @param mixed $themeColor
     */
    public function setThemeColor($themeColor)
    {
        $this->themeColor = $themeColor;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public static function firstById($id){
      return static::simpleQuery()->where("id",$id)->first();
    }
	
}