<?php 
namespace App\Repos;

use DB;
use CB;

class Goalscorers extends BaseRepo
{
	public static $tableName = "goalscorers";

    
	private $id;
	private $matchResults;
	private $players;
	private $goalTime;
	private $schedules;
	private $teams;
	private $createdAt;
	private $updatedAt;
	private $deletedAt;


    
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	/**
	* @return MatchResults
	*/
	public function getMatchResults() {
		return $this->matchResults;
	}

	public function setMatchResults($matchResults) {
		$this->matchResults = $matchResults;
	}

	/**
	* @return Players
	*/
	public function getPlayers() {
		return Players::findById($this->players);
	}

	public function setPlayers($players) {
		$this->players = $players;
	}

	public function getGoalTime() {
		return $this->goalTime;
	}

	public function setGoalTime($goalTime) {
		$this->goalTime = $goalTime;
	}

	/**
	* @return Schedules
	*/
	public function getSchedules() {
		return Schedules::findById($this->schedules);
	}

	public function setSchedules($schedules) {
		$this->schedules = $schedules;
	}

	/**
	* @return Teams
	*/
	public function getTeams() {
		return Teams::findById($this->teams);
	}

	public function setTeams($teams) {
		$this->teams = $teams;
	}

	public function getCreatedAt() {
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;
	}

	public function getUpdatedAt() {
		return $this->updatedAt;
	}

	public function setUpdatedAt($updatedAt) {
		$this->updatedAt = $updatedAt;
	}

	public function getDeletedAt() {
		return $this->deletedAt;
	}

	public function setDeletedAt($deletedAt) {
		$this->deletedAt = $deletedAt;
	}


}