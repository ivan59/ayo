<?php 
namespace App\Repos;

use DB;
use CB;

class Schedules extends BaseRepo
{
	public static $tableName = "schedules";

    
	private $id;
	private $matchDate;
	private $matchTime;
	private $homeTeam;
	private $homeTeamGoals;
	private $guestTeam;
	private $guestTeamGoals;
	private $createdAt;
	private $updatedAt;
	private $deletedAt;


    
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getMatchDate() {
		return $this->matchDate;
	}

	public function setMatchDate($matchDate) {
		$this->matchDate = $matchDate;
	}

	public function getMatchTime() {
		return $this->matchTime;
	}

	public function setMatchTime($matchTime) {
		$this->matchTime = $matchTime;
	}

	public function getHomeTeam() {
		return $this->homeTeam;
	}

	public function setHomeTeam($homeTeam) {
		$this->homeTeam = $homeTeam;
	}

	public function getHomeTeamGoals() {
		return $this->homeTeamGoals;
	}

	public function setHomeTeamGoals($homeTeamGoals) {
		$this->homeTeamGoals = $homeTeamGoals;
	}

	public function getGuestTeam() {
		return $this->guestTeam;
	}

	public function setGuestTeam($guestTeam) {
		$this->guestTeam = $guestTeam;
	}

	public function getGuestTeamGoals() {
		return $this->guestTeamGoals;
	}

	public function setGuestTeamGoals($guestTeamGoals) {
		$this->guestTeamGoals = $guestTeamGoals;
	}

	public function getCreatedAt() {
		return $this->createdAt;
	}

	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;
	}

	public function getUpdatedAt() {
		return $this->updatedAt;
	}

	public function setUpdatedAt($updatedAt) {
		$this->updatedAt = $updatedAt;
	}

	public function getDeletedAt() {
		return $this->deletedAt;
	}

	public function setDeletedAt($deletedAt) {
		$this->deletedAt = $deletedAt;
	}


}