<?php 
namespace App\Repos;

use DB;
use CB;

class MatchResultStatus extends BaseRepo
{
	public static $tableName = "match_result_status";

    
	private $id;
	private $schedules;
	private $teams;
	private $status;


    
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	/**
	* @return Schedules
	*/
	public function getSchedules() {
		return Schedules::findById($this->schedules);
	}

	public function setSchedules($schedules) {
		$this->schedules = $schedules;
	}

	/**
	* @return Teams
	*/
	public function getTeams() {
		return Teams::findById($this->teams);
	}

	public function setTeams($teams) {
		$this->teams = $teams;
	}

	public function getStatus() {
		return $this->status;
	}

	public function setStatus($status) {
		$this->status = $status;
	}


}