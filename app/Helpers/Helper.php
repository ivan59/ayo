<?php

use Illuminate\Support\Facades\Storage;

// example helper method
if(!function_exists('format_date')){
    function format_date($date){
        return date('Y-m-d', strtotime($date));
    }
}

if(!function_exists('redirect_back')){
    function redirect_back($message,$type){
        $res = redirect()->back()->with(["message"=>$message,'message_type'=>$type])->withInput();\Session::driver()->save();$res->send();exit();
    }
}