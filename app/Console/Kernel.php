<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use CRUDBooster;
use App\Repos\BlastNotification;
use App\Repos\CmsUsers;
use App\Repos\ListNotifications;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){
            // detect tidak masuk
            if(get_master('Maksimal Jam Checkout') <= date('H:i:s')){
                without_explaination('');
            }
            // blast notification
            $blast = BlastNotification::getByDate("");
            if(count($blast)!=0){
                foreach($blast as $b){
                    $regID  = CmsUsers::getPluck("");
                    $datas  = ['title'=>'blast notification','content'=>$b->description,"type"=>"blast_notification"];
                    // push android
                    $android    = CRUDBooster::sendFCM($regID,$datas);
                    // push ios
                    // $ios = CRUDBooster::sendFCMIOS($regID,$datas);

                    $u = BlastNotification::findById($b->id);
                    $u->setIsBlast(1);
                    $u->save();

                    // save list notification
                    $users = CmsUsers::getData('');
                    if(count($users)!=0){
                        foreach($users as $u){
                            $insert = new ListNotifications;
                            $insert->setCmsUsers($u->id);
                            $insert->setDate($b->datetime);
                            $insert->setDescription($b->description);
                            $insert->setIsRead(0);
                            $insert->setCreatedAt(now(''));
                            $insert->save();
                        }
                    }
                }
            }
            // push before 15 minutes checkin 
            if(now('' ) >= subtraction_datetime('') && now('') < subtraction_datetime_max('')){
                $regID  = CmsUsers::getPluck("");
                $datas  = ['title'=>'15 menit lagi checkin','content'=>'15 menit lagi checkin, mohon untuk bersiap',"type"=>"before_checkin"];
                // push android
                $p_android    = CRUDBooster::sendFCM($regID,$datas);
                // push ios
                // $p_ios    = CRUDBooster::sendFCMIOS($regID,$datas);

            }
        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
