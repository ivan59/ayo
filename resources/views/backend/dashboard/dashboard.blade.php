@extends('crudbooster::admin_template')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

<div class="box box-default">
    <div class="box-header">
      <div class="row">
        <div class="col-md-4">
          <div class="box row_one box-danger box-png">
            <div class="box-header ui-sortable-handle">
              <div class="row">
                <div class="col-md-8 box-title">
                  <p class="title">Total Teams</p>
                  <p class="number">{{number_format($countTeams)}}</p>
                </div>
                <div class="col-md-4">
                  <img class="image" src="{{asset('image/icon-team-football.png')}}">
                </div>
                <div class="col-md-12 box-title">
                  <p class="view-detail"><a href="{{asset('admin/teams')}}">View Detail</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="box row_one box-danger box-png">
            <div class="box-header ui-sortable-handle">
              <div class="row">
                <div class="col-md-8 box-title">
                  <p class="title">Total Schedules</p>
                  <p class="number">{{number_format($countSchedules)}}</p>
                </div>
                <div class="col-md-4">
                  <img class="image" src="{{asset('image/icon-schedules.png')}}">
                </div>
                <div class="col-md-12 box-title">
                  <p class="view-detail"><a href="{{asset('admin/schedules')}}">View Detail</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="box row_one box-danger box-png">
            <div class="box-header ui-sortable-handle">
              <div class="row">
                <div class="col-md-8 box-title">
                  <p class="title">Total Players</p>
                  <p class="number">{{number_format($countPlayers)}}</p>
                </div>
                <div class="col-md-4">
                  <img class="image" src="{{asset('image/icon-player.png')}}">
                </div>
                <div class="col-md-12 box-title">
                  <p class="view-detail"><a href="{{asset('admin/teams')}}">View Detail</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="box row_one">
            <div class="box-header ui-sortable-handle">
              <div class="row">
                <div class="col-md-8 box-title">
                  <p class="title">Schedule Today ( {{date('d M Y')}} )</p>
                </div>
                <div class="col-md-12">
                   <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Home Team</th>
                        <th>Guest Team</th>
                        <th>Match Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(!$schedulesToday->isEmpty())
                        @foreach($schedulesToday as $schedule)
                          @php
                            $homeTeam = \App\Repos\Teams::findByIdCustom($schedule->home_team)->team_name;
                            $guestTeam = \App\Repos\Teams::findByIdCustom($schedule->guest_team)->team_name;
                          @endphp 
                          <tr>
                            <td>{{$homeTeam}}</td>
                            <td>{{$guestTeam}}</td>
                            <td>{{date('d M Y', strtotime($schedule->match_date))}} {{date('H:i:s', strtotime($schedule->match_time))}}</td>
                          </tr>
                        @endforeach
                      @else
                        <tr>
                          <td colspan="3" align="center">No schedule today</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box row_one">
            <div class="box-header ui-sortable-handle">
              <div class="row">
                <div class="col-md-8 box-title">
                  <p class="title">Top Scorer</p>
                </div>
                <div class="col-md-12">
                   <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Player</th>
                        <th>Total Goals</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(!$topScorer->isEmpty())
                        @foreach($topScorer as $top)
                          <tr>
                            <td>{{$no++}}</td>
                            <td>{{$top->name}}</td>
                            <td>{{number_format($top->total)}}</td>
                          </tr>
                        @endforeach
                      @else
                        <tr>
                          <td colspan="2" align="center">No top scorer</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection