<p>	
    <a href="{{$url_data}}"><i class="fa fa-chevron-circle-left "></i> Back</a>
</p>

<div class="box box-default">
    <div class="box-header">
        @if($type == 'player')
            <h1 class="box-title">Team : <b>{{$team->getTeamName()}}</b></h1>
        @else
            <h1 class="box-title">List of goalscorers on <b>{{$home_team}}</b></h1>
            <hr>
            <div class="alert alert-warning" role="alert">
                <b><i>In the match of {{$home_team}} VS {{$guest_team}} on {{date('d M Y', strtotime($schedules->getMatchDate()))}} {{$schedules->getMatchTime()}}</i></b>
            </div>
        @endif
    </div>
</div>