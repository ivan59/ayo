<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSomeNewTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('team_name',100)->nullable();
            $table->string('team_logo',100)->nullable();
            $table->string('since',4)->nullable();
            $table->text('team_address')->nullable();
            $table->string('team_city',30)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100)->nullable();
            $table->string('height',4)->nullable();
            $table->string('weight',4)->nullable();
            $table->string('position',20)->nullable();
            $table->string('back_number',4)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->date('match_date')->nullable();
            $table->time('match_time')->nullable();
            $table->integer('home_team')->comment('by teams table id')->nullable();
            $table->integer('guest_team')->comment('by teams table id')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::create('match_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('final_total_score',20)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::create('goalscorers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_results_id');
            $table->integer('players_id');
            $table->dateTime('goal_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
